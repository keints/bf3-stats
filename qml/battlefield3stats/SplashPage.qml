import QtQuick 1.1
import com.nokia.meego 1.0
import "Storage.js" as Storage

MyPage {
    id: splashPage
    property bool showToolbar: false
    property bool completed: false

    property string name: "splash"

    function getNextPage() {
        var pageName = (rootElement.soldiers && rootElement.soldiers.length) ? "overview" : "soldier";
        return rootElement.getPage(pageName);
    }

    Component.onCompleted: {
        Storage.initialize();
        Storage.insertData();

        // set initial "currentSoldier" and "soldiers" attributes to PageStackWindow
        rootElement.refreshSoldiersData(true);

        completed = true;
    }

    Image {
        id: splashImage
        anchors.fill: parent

        source: "img/splash_screen.jpg"
        sourceSize.width: 480
        sourceSize.height: 854

        width: 480
        height: 854
    }

    Timer {
        id: timer
        interval: 50
        running: true
        repeat: true

        property int running_time: 0

        onTriggered: {
            running_time += interval;
            if (running_time > 200) { // 0.2 seconds
                if (parent.completed) {
                    running = false;
                    pageStack.replace(splashPage.getNextPage(), {}, true);
                }
            }
        }
    }
}
