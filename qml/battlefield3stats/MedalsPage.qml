import QtQuick 1.1
import com.nokia.meego 1.0
import "UIConstants.js" as UIConstants
import "Utils.js" as Utils

MyPage {
    id: medalsPage
    tools: singlePageTools

    property variant medalsData: [];

    SoldierHeaderBar {
        id: header
        z: 100
    }

    Item {
        id: medalsGridRoot
        anchors.top: header.bottom

        width: parent.width
        height: parent.height - UIConstants.ToolbarHeight

        Component {
            id: medalDelegate

            Item {
                width: 240
                height: 220

                function getProgressbarWidth() {
                    var maxWidth = medalProgressBar.width;

                    if (!model.data.needed) {
                        return 0;

                    } else if (model.data.curr == model.data.needed) {
                        return maxWidth;

                    } else {

                        var percent = (model.data.curr * 100) / model.data.needed;
                        var progressWidth = (maxWidth * percent) / 100;

                        return Math.min(maxWidth, progressWidth);
                    }

                }

                Image {
                    id: medalImage
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter

                    opacity: model.data.count > 0 ? 1 : 0.3

                    source: "img/small/" + model.data.img_large
                    sourceSize.width: 206
                    sourceSize.height: 206

                    width: 206
                    height: 206
                }

                Rectangle {
                    id: countBackground
                    y: medalBackground.y + 8
                    x: medalBackground.x + medalBackground.width - width - 8
                    width: 0
                    height: 25
                    color: "#545454"

                    opacity: 0.8
                    visible: model.data.count > 1 ? true : false
                }

                Text {
                    anchors.verticalCenter: countBackground.verticalCenter
                    anchors.horizontalCenter: countBackground.horizontalCenter
                    color: "white"
                    font.pixelSize: 17

                    visible: model.data.count > 1 ? true : false

                    text: "x" + model.data.count

                    Component.onCompleted: {
                        countBackground.width = width + 15
                    }
                }

                Rectangle {
                    id: medalBackground

                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    width: parent.width - 20
                    height: parent.height

                    color: "gray"
                    opacity: 0
                }

                MouseArea {
                    anchors.fill: medalBackground

                    onClicked: {
                        var medalDialogComponent = Qt.createComponent("MedalDialog.qml");
                        var medalDialogObj = medalDialogComponent.createObject(medalsPage, {medal: model.data});
                        medalDialogObj.open();
                    }
                    onPressed: { medalBackground.opacity = 0.3 }
                    onReleased: { medalBackground.opacity = 0 }

                    onContainsMouseChanged: {
                        if (!containsMouse) {
                            medalBackground.opacity = 0
                        }
                    }
                }

                Rectangle {
                    id: medalProgressBar
                    anchors.bottom: medalBackground.bottom
                    anchors.left: medalBackground.left
                    width: medalBackground.width
                    height: 5
                    color: "gray"
                    opacity: 0.5
                }
                Rectangle {
                    anchors.bottom: medalProgressBar.bottom
                    anchors.left: medalProgressBar.left
                    width: parent.getProgressbarWidth()
                    height: medalProgressBar.height
                    color: "#59cb5c"
                }
            }
        }

        GridView {
            id: medalListView
            anchors.fill: parent
            model: MedalListModel { medals: medalsPage.medalsData}
            delegate: medalDelegate
            cellWidth: 240
            cellHeight: 230
            cacheBuffer: 3450 // cache 15 rows both sides for smooth scrolling
            header: Fieldset {
                title: "Medals"
            }
        }

        ScrollDecorator { flickableItem: medalListView }
    }
}
