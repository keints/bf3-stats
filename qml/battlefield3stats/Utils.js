// add spaces to separate thousands
function beautifyNumber(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ' ' + '$2');
    }
    return x1 + x2;
}

// format time in like this "4D 21H 12M"
function humanTime(timeInSeconds) {
    var minutes = Math.floor(timeInSeconds / 60);
    var hours = Math.floor(minutes / 60);
    var days = Math.floor(hours / 24);

    var remainingHours = hours - (days * 24);
    var remainingMinutes = minutes - (days * 24 * 60) - (remainingHours * 60);

    var timeString = "";
    if (days) {
        timeString += days + "D "
    }
    timeString += remainingHours + "H "
    timeString += remainingMinutes + "M"

    return timeString;
}

// calculate accuracy according to shots and hits and return nicely formatted string
function getAccuracy(shots, hits) {
    var value = 0;
    if (shots > 0) {
        value = (100 * hits) / shots;
    }
    value += ""; // convert to string

    var commaPosition = value.indexOf(".");
    if (commaPosition != -1) {
        if (value.substr(commaPosition + 1, 1) != "0") {
            value = value.substr(0, commaPosition + 2);
        } else {
            // don't show comma places if first place after comma is zero
            value = value.substr(0, commaPosition);
        }
    }

    return value + "%"
}

// get formatted date string
function getDateString(timestamp) {
    var someDate = new Date(timestamp * 1000);
    var monthChunk = someDate.getMonth() + 1;
    monthChunk = monthChunk < 10 ? '0' + monthChunk : monthChunk;
    var dateChunk = someDate.getDate() < 10 ? '0' + someDate.getDate() : someDate.getDate();
    return someDate.getFullYear() + '-' + monthChunk + '-' + dateChunk;
}

// make first letter of string uppercase
function ucFirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
