import QtQuick 1.0
import "Utils.js" as Utils

Item {
    id: scoreDetailsRoot

    property int score_kits: 0 // assault, engineer, recon, support
    property int score_vehicles: 0 // vehicleaa, vehicleah, vehicleifv, vehiclejet, vehiclembt, vehiclesh
    property int score_award: 0 // award
    property int score_unlocks: 0 // unlock

    width: parent.width
    height: 290

    Grid {
        id: scoreLayout

        width: parent.width

        function getScores() {
            var totalScore = (parent.score_kits + parent.score_vehicles + parent.score_award + parent.score_unlocks);
            return [
                { title: "Kits", value: Utils.beautifyNumber(parent.score_kits) + " XP" },
                { title: "Vehicles", value: Utils.beautifyNumber(parent.score_vehicles) + " XP" },
                { title: "Award", value: Utils.beautifyNumber(parent.score_award) + " XP" },
                { title: "Unlocks", value: Utils.beautifyNumber(parent.score_unlocks) + " XP" },
                { title: "Total", value: Utils.beautifyNumber(totalScore) + " XP", bold: true }
            ];
        }

        columns: 1
        spacing: 10

        Component {
            id: scoreRow

            DataRow {
                title: modelData.title
                value: modelData.value
                bold: modelData.bold ? true : false
            }
        }

        Repeater {
            id: scoreDetailsRepeater
            model: scoreLayout.getScores()
            delegate: scoreRow
        }
    }
}
