import QtQuick 1.0

Item {
    width: parent.width
    height: 50

    property string title: ""
    property string value: ""
    property bool bold: false

    Rectangle {
        id: rowBackground
        anchors.fill: parent
        radius: 9
        color: "gray"
        opacity: 0.1
    }

    Text {
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: 12

        font.capitalization: Font.AllUppercase
        font.pixelSize: 20
        font.bold: parent.bold
        color: "grey"

        text: parent.title
    }

    Text {
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 12

        font.capitalization: Font.AllUppercase
        font.pixelSize: 20
        color: "white"

        text: parent.value
    }
}
