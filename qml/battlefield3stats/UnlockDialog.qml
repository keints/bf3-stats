import QtQuick 1.1
import com.nokia.meego 1.0
import "Utils.js" as Utils

Dialog {
    id: unlockDialog
    width: parent.width - 40
    anchors.verticalCenter: parent.verticalCenter

    property variant unlock: null

    title: Item {
        id: unlockDialogTitle

        width: parent.width
        height: 55

        MarqueeText {
            id: unlockDialogTitleText

            width: parent.width
            font.capitalization: Font.AllUppercase
            font.family: "Nokia Pure"
            font.pixelSize: 30
            font.letterSpacing: -1
            color: "white"

            textAlign: "left"
            interval: 100

            text: unlockDialog.unlock ? unlockDialog.unlock.name : ""
        }

        Rectangle {
            anchors.top: unlockDialogTitleText.bottom
            anchors.topMargin: 2
            width: parent.width
            height: 1
            color: "gray"
        }
    }
    content: Item {
        height: unlockDialogTitle.height + unlockDialogDesc.height + progressRow.height

        Text {
            id: unlockDialogDesc
            anchors.topMargin: 5
            width: unlockDialog.width

            font.family: "Nokia Pure"
            font.pixelSize: 20
            font.letterSpacing: -1
            color: "gray"

            horizontalAlignment: Text.AlignJustify
            wrapMode: Text.WordWrap

            text: unlockDialog.unlock ? unlockDialog.unlock.desc : ""
        }

        DataRow {
            id: progressRow

            function getTitle() {
                if (unlockDialog.unlock) {
                    if (unlockDialog.unlock.curr == unlockDialog.unlock.needed) {
                        return "Allready unlocked";
                    } else {
                        return "Vehicle Class Score";
                    }
                }
                return "";
            }

            function getValue() {
                if (unlockDialog.unlock) {
                    if (unlockDialog.unlock.curr == unlockDialog.unlock.needed) {
                        return "";
                    } else {
                        return Utils.beautifyNumber(unlockDialog.unlock.curr) + " XP / " + Utils.beautifyNumber(unlockDialog.unlock.needed) + " XP";
                    }
                } else {
                    return "";
                }
            }

            width: unlockDialog.width
            anchors.top: unlockDialogDesc.bottom
            anchors.topMargin: 10

            title: getTitle()
            value: getValue()
        }
    }

    buttons: ButtonRow {
        style: ButtonStyle { }
        anchors.horizontalCenter: parent.horizontalCenter
        Button { text: "Close"; onClicked: unlockDialog.close() }
    }

    onStatusChanged: {
        if (status == DialogStatus.Closing) {
            unlockDialog.destroy();
        }
    }
}
