import QtQuick 1.1
import com.nokia.meego 1.0

Item {
    z: 200
    anchors.fill: parent

    Rectangle {
        anchors.fill: parent
        color: "black"
        opacity: 0.8
    }

    BusyIndicator {
        id: busyIndicator
        running: true

        platformStyle: BusyIndicatorStyle { size: "medium" }

        anchors.horizontalCenter: parent.horizontalCenter
        y: 380
    }

    Text {
        color: "white"
        text: "Loading.."

        font.family: "Nokia Pure"
        font.pixelSize: 21

        anchors.top: busyIndicator.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 18
    }
}
