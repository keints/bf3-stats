import QtQuick 1.0
import "Utils.js" as Utils

Item {
    id: rankRoot
    property int nr: 44
    property string name: "MAJOR"
    property string image: "ranksmedium/r44.png"
    property int score: 1460000

    property int score_next: 1520000
    property int left_next: 11010
    property int rank_progress: 0
    visible: false

    Behavior on rank_progress {
        NumberAnimation { duration: getAnimationDuration(); easing.type: Easing.OutCubic }
    }

    Component.onCompleted: {
        rank_progress = score;
        visible = true;
    }

    function getAnimationDuration() {
        if (score > 0) {
            var baseDuration = 1000;

            var levelScoreRange = score_next - score;
            var scoreProgress = score_next - left_next - score;

            var percent = (scoreProgress * 100) / levelScoreRange;

            return (baseDuration * percent) / 100;
        }
        return 0;
    }

    function getProgressBarWidth() {
        if (score > 0) {
            var levelScoreRange = score_next - score;
            var scoreProgress = score_next - left_next - score;

            var percent = (scoreProgress * 100) / levelScoreRange;

            var progressBarWidth = Math.round((rankBar.width * percent) / 100);
            return Math.min(progressBarWidth, rankBar.width);
        }
        return 0;
    }

    onVisibleChanged: {
        if (visible) {
            rank_progress = score_next - left_next;
        } else {
            rank_progress = score;
        }
    }

    width: parent.width - 10
    height: 119

    Image {
        id: rankLogo
        anchors.top: parent.top
        anchors.topMargin: 4
        anchors.left: parent.left

        source: "img/" + parent.image
        sourceSize.width: 128
        sourceSize.height: 128

        width: 128
        height: 128

        fillMode: Image.PreserveAspectFit
        smooth: true

        Behavior on width {
            NumberAnimation { duration: 500; easing.type: Easing.OutCubic }
        }

        onVisibleChanged: {
            if (rankRoot.visible) {
                width = 128
            } else {
                width = 100
            }
        }
    }

    Text {
        id: rankName
        color: "white"
        font.capitalization: Font.AllUppercase
        font.weight: Font.Bold
        font.pixelSize: 24
        text: parent.name

        anchors.top: parent.top
        anchors.left: rankLogo.right
        anchors.leftMargin: 10
    }

    Text {
        id: rankNum
        color: "grey"
        font.capitalization: Font.AllUppercase
        font.weight: Font.Light
        font.pixelSize: 19
        text: "RANK " + parent.nr

        anchors.top: rankName.bottom
        anchors.topMargin: 5
        anchors.left: rankName.left
    }

    Rectangle {
        id: rankBar
        border.color: "white"
        border.width: 4

        color: "lightgrey"

        height: 15

        anchors.left: rankName.left
        anchors.right: parent.right
        anchors.top: rankNum.bottom
        anchors.topMargin: 10

        Rectangle {
            id: rankBarIndicator
            color: "orange"

            anchors.margins: 1
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: parent.width / 2

            Behavior on width {
                NumberAnimation { duration: rankRoot.getAnimationDuration(); easing.type: Easing.OutCubic }
            }

            onVisibleChanged: {
                if (rankRoot.visible) {
                    width = rankRoot.getProgressBarWidth()
                } else {
                    width = 0
                }
            }

        }
    }

    Text {
        id: rankScoreProgress
        color: "white"

        font.pixelSize: 17
        text: Utils.beautifyNumber(parent.rank_progress) + " XP" // (parent.score_next - parent.left_next)

        anchors.top: rankBar.bottom
        anchors.topMargin: 12
        anchors.left: rankName.left
    }

    Text {
        id: nextRankScore
        color: "gray"

        font.pixelSize: 17
        text: " / " + Utils.beautifyNumber(parent.score_next) + " XP"

        anchors.top: rankScoreProgress.top
        anchors.left: rankScoreProgress.right
    }
}
