import QtQuick 1.1

ListModel {
    id: vehicleListModel

    property variant medals: []

    function createListElements() {
        for (var i = 0; i < this.medals.length; i++) {
            vehicleListModel.append({"data": this.medals[i]});
        }
    }

    Component.onCompleted: {
        createListElements();
    }
}
