import QtQuick 1.0

Item {
    id: marqueeText
    height: scrollingText.height
    clip: true

    property int tempX: 0
    property alias text: scrollingText.text
    property alias font: scrollingText.font
    property alias color: scrollingText.color
    property string textAlign: "center"
    property int interval: 150

    Text {
        id: scrollingText
        x: (scrollingText.width > marqueeText.width || textAlign == "left") ? tempX : ((marqueeText.width - scrollingText.width) / 2)
    }

    Timer {
        id: timer
        interval: marqueeText.interval
        running: (scrollingText.width > marqueeText.width)
        repeat: true
        onTriggered: {
            tempX = tempX + 2
            scrollingText.x = -tempX;

            if(tempX + marqueeText.width > scrollingText.width) {
                timer.running = false;
                pauseTimer.running = true;
            }
        }
    }

    Timer {
        id: pauseTimer
        interval: 1500; running: false; repeat: false
        onTriggered: {
            delayTimer.running = true;
            scrollingText.x = 0;
            marqueeText.tempX = 0;
        }
    }

    Timer {
        id: delayTimer
        interval: 1500; running: false; repeat: false
        onTriggered: {
            timer.running = true;
        }
    }
}
