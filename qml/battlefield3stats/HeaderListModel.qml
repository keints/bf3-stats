import QtQuick 1.1

ListModel {
    id: headerListModel

    property variant soldiers: []

    function createListElements() {
        for (var i = 0; i < this.soldiers.length; i++) {
            headerListModel.append(this.soldiers[i]);
        }
    }

    Component.onCompleted: {
        createListElements();
    }
}
