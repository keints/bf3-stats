import QtQuick 1.1
import com.nokia.meego 1.0

SelectionDialog {
    id: soldierSelectionDialog

    function updateHeader() {
        rootElement.changeCurrentSoldier(soldierSelectionDialog.model.get(selectedIndex).id);
    }

    function getSelectedIndex() {
        var soldiers = model.soldiers;
        for (var i = 0; i < soldiers.length; i++) {
            if (soldiers[i].current == '1') {
                return i;
            }
        }
        return 0;
    }

    titleText: "Select soldier"
    selectedIndex: getSelectedIndex()

    onAccepted: {
        updateHeader();
    }

    onStatusChanged: {
        if (status == DialogStatus.Closing) {
            model.destroy();
            soldierSelectionDialog.destroy();
        }
    }
}
