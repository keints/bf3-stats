import QtQuick 1.1

ListModel {
    id: vehicleListModel

    function getVehicles() {
        var data = rootElement.currentSoldier.data;

        // convert hash to array
        var vehicles = [];
        if (data && data.stats && data.stats.vehicles) {
            for (var key in data.stats.vehicles) {
                vehicles.push(data.stats.vehicles[key]);
            }
        }

        // reorder vehicles, vehicles with most kills first
        for (var i = 0; i < vehicles.length; i++) {
            for (var j = i; j < vehicles.length; j++) {
                if (vehicles[j].kills > vehicles[i].kills) {
                    var tmp = vehicles[i];
                    vehicles[i] = vehicles[j];
                    vehicles[j] = tmp;
                }
            }
        }

        // suborder vehicles by time
        for (var i = 0; i < vehicles.length; i++) {
            for (var j = i; j < vehicles.length; j++) {
                if (vehicles[j].kills == vehicles[i].kills && vehicles[j].time > vehicles[i].time) {
                    var tmp2 = vehicles[i];
                    vehicles[i] = vehicles[j];
                    vehicles[j] = tmp2;
                }
            }
        }

        return vehicles;
    }

    function createListElements() {
        var vehicles = getVehicles();
        for (var i = 0; i < vehicles.length; i++) {
            vehicleListModel.append({"data": vehicles[i]});
        }
    }

    Component.onCompleted: {
        createListElements();
    }
}
