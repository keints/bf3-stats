import QtQuick 1.1
import com.nokia.meego 1.0

Dialog {
    id: aboutDialog
    width: parent.width - 60
    anchors.verticalCenter: parent.verticalCenter

    title: Item {
        id: aboutDialogTitle

        width: parent.width
        height: 55

        MarqueeText {
            id: aboutDialogTitleText

            width: parent.width
            font.capitalization: Font.AllUppercase
            font.family: "Nokia Pure"
            font.pixelSize: 30
            font.letterSpacing: -1
            color: "white"

            textAlign: "left"
            interval: 100

            text: "About"
        }

        Rectangle {
            anchors.top: aboutDialogTitleText.bottom
            anchors.topMargin: 2
            width: parent.width
            height: 1
            color: "gray"
        }
    }
    content: Item {
        height: aboutDialogTitle.height + aboutDialogDesc.height

        Text {
            id: aboutDialogDesc
            anchors.topMargin: 10
            width: aboutDialog.width

            font.family: "Nokia Pure"
            font.pixelSize: 21
            font.letterSpacing: -1
            color: "gray"

            horizontalAlignment: Text.AlignJustify
            wrapMode: Text.WordWrap

            textFormat: Text.RichText

            text: '<style type="text/css">a:link { color: #fff; text-decoration: none; } p { margin-bottom: 20; }</style>' +
            '<p>"BattleStats" is unofficial app for tracking Battlefield 3 soldier statistics, ' +
            'developed by Avo Keinaste.</p>' +

            '<p>Statistics data is provided by <a href=\"http://bf3stats.com/\">bf3stats.com</a> API. ' +
            'If you have any questions or wish to contact me for other reasons, send an e-mail to <a href="mailto:keints@hotmail.com">keints@hotmail.com</a></p>' +

            '<p>All Battlefield franchise related trademarks are the property of their respective owners.</p>'
        }

    }

    buttons: ButtonRow {
        style: ButtonStyle { }
        anchors.horizontalCenter: parent.horizontalCenter
        Button {
            text: "Close";
            onClicked: aboutDialog.close();
        }
    }

    onStatusChanged: {
        if (status == DialogStatus.Closing) {
            aboutDialog.destroy();
        }
    }
}
