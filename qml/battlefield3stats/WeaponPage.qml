import QtQuick 1.1
import com.nokia.meego 1.0
import "UIConstants.js" as UIConstants
import "Utils.js" as Utils

MyPage {
    id: weaponPage
    tools: singlePageTools

    property variant weaponData: null

    SoldierHeaderBar {
        id: header
        z: 100
    }

    Flickable {
        id: pageContent
        width: parent.width
        height: parent.height - UIConstants.ToolbarHeight

        anchors.top: header.bottom

        contentWidth: parent.width
        contentHeight: 0

        Item {
            id: flickableMargin

            onHeightChanged: {
                pageContent.contentHeight = height
            }

            anchors.top: parent.top
            anchors.left: parent.left
            anchors.leftMargin: UIConstants.DefaultMargin
            anchors.right: parent.right
            anchors.rightMargin: UIConstants.DefaultMargin

            height: weaponImageFieldset.height + weaponImageBackground.height + nameDescriptionFieldset.height + weaponName.height + 60

            Fieldset {
                id: weaponImageFieldset
                title: "Image"
            }

            Rectangle {
                id: weaponImageBackground
                color: "gray"
                opacity: 0.1
                width: parent.width
                height: 270
                radius: 9
                anchors.top: weaponImageFieldset.bottom
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Image {
                id: weaponImage
                anchors.verticalCenter: weaponImageBackground.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter

                source: "img/" + weaponPage.weaponData.img
                sourceSize.width: 426
                sourceSize.height: 256

                width: 426
                height: 256
            }

            Fieldset {
                id: nameDescriptionFieldset
                title: "Name & Description"
                anchors.top: weaponImageBackground.bottom
            }

            Text {
                id: weaponName
                anchors.top: nameDescriptionFieldset.bottom
                anchors.left: parent.left

                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                clip: true

                font.family: "Nokia Pure"
                font.pixelSize: 30
                font.letterSpacing: -1
                color: "white"

                wrapMode: Text.WordWrap

                text: weaponData.name
            }

            Image {
                id: kitLogo
                anchors.left: weaponName.right
                anchors.leftMargin: 10
                anchors.verticalCenter: weaponName.verticalCenter

                visible: (weaponData.kit != "general")

                source: weaponData.kit != "general" ? "img/kit/" + weaponData.kit + ".png" : ""
                sourceSize.width: 50
                sourceSize.height: 50

                width: 28
                height: 28

                fillMode: Image.PreserveAspectFit
                smooth: true
            }

            Text {
                id: weaponDesc
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: weaponName.bottom

                horizontalAlignment: Text.AlignJustify
                verticalAlignment: Text.AlignVCenter
                clip: true

                font.family: "Nokia Pure"
                font.pixelSize: 20
                font.letterSpacing: -1
                color: "gray"

                wrapMode: Text.WordWrap

                text: weaponData.desc

                Component.onCompleted: {
                    flickableMargin.height = flickableMargin.height + height
                }
            }

            Fieldset {
                id: propertiesFieldset
                title: "Properties"
                anchors.top: weaponDesc.bottom
                visible: (weaponProperties.getProperties().length > 0)

                Component.onCompleted: {
                    if (weaponProperties.getProperties().length > 0) {
                        flickableMargin.height = flickableMargin.height + height
                    }
                }
            }

            Column {
                id: weaponProperties
                anchors.top: propertiesFieldset.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width

                visible: (getProperties().length > 0)

                spacing: 8

                function getProperties() {
                    var fireModes = [];
                    if (weaponData.fireModeSingle) {
                        fireModes.push('single');
                    }
                    if (weaponData.fireModeBurst) {
                        fireModes.push('burst');
                    }
                    if (weaponData.fireModeAuto) {
                        fireModes.push('auto');
                    }
                    var fireModesStr = fireModes.join(', ');

                    var properties = [];
                    if (weaponData.ammo) {
                        properties.push({title: 'Ammo', value: weaponData.ammo});
                    }
                    if (fireModesStr != "") {
                        properties.push({title: 'Fire modes', value: fireModesStr});
                    }
                    if (weaponData.rateOfFire) {
                        properties.push({title: 'Rate of Fire', value: weaponData.rateOfFire});
                    }
                    if (weaponData.range) {
                        properties.push({title: 'Range', value: weaponData.range});
                    }

                    return properties;
                }

                Component {
                    id: propertiesRow

                    Item {
                        width: weaponProperties.width
                        height: 50

                        Rectangle {
                            id: rowBackground
                            anchors.fill: parent
                            radius: 9
                            color: "gray"
                            opacity: 0.1
                        }

                        Text {
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.left: parent.left
                            anchors.leftMargin: 12

                            font.capitalization: Font.AllUppercase
                            font.pixelSize: 20
                            color: "grey"

                            text: modelData.title
                        }

                        Text {
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.right: parent.right
                            anchors.rightMargin: 12

                            font.capitalization: Font.AllUppercase
                            font.pixelSize: 20
                            color: "white"

                            text: modelData.value
                        }
                    }
                }

                Repeater {
                    id: propertiesRowRepeater
                    model: weaponProperties.getProperties()
                    delegate: propertiesRow

                    onItemAdded: {
                        var addedHeight = itemAt(index).height;
                        if (index > 0) {
                            addedHeight += weaponProperties.spacing;
                        }
                        flickableMargin.height = flickableMargin.height + addedHeight
                    }
                }
            }

            Fieldset {
                id: statisticsFieldset
                title: "Statistics"
                anchors.top: weaponProperties.visible ? weaponProperties.bottom : weaponDesc.bottom

                Component.onCompleted: {
                    flickableMargin.height = flickableMargin.height + height
                }
            }

            Grid {
                id: weaponStatistics
                anchors.top: statisticsFieldset.bottom
                width: parent.width
                height: 200

                columns: 3
                rows: 2
                spacing: 10

                // calculate infoblock width
                function getBlockWidth() {
                    return (width - ((spacing * (columns - 1)))) / columns;
                }

                // calculate infoblock height
                function getBlockHeight() {
                    return (height - ((spacing * (rows - 1)))) / rows;
                }

                function getStatistics() {
                    return [
                        {title: "Time Used", value: Utils.humanTime(weaponData.time), font_size: 28 },
                        {title: "Kills", value: Utils.beautifyNumber(weaponData.kills), font_size: 28 },
                        {title: "Accuracy", value: Utils.getAccuracy(weaponData.shots, weaponData.hits) },
                        {title: "Shots", value: Utils.beautifyNumber(weaponData.shots), font_size: 27},
                        {title: "Hits", value: Utils.beautifyNumber(weaponData.hits), font_size: 27},
                        {title: "Headshots", value: Utils.beautifyNumber(weaponData.headshots)}
                    ];
                }

                Component {
                    id: statisticsBox

                    Item {
                        width: weaponStatistics.getBlockWidth()
                        height: weaponStatistics.getBlockHeight()

                        Rectangle {
                            anchors.fill: parent
                            color: "gray"
                            opacity: 0.1
                            radius: 9
                        }

                        Text {
                            id: boxValue
                            width: parent.width
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.top: parent.top
                            anchors.topMargin: 3
                            anchors.bottom: boxTitle.top
                            anchors.bottomMargin: 3

                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            clip: true

                            font.family: "Nokia Pure"
                            font.pixelSize: modelData.font_size ? modelData.font_size : 35
                            font.letterSpacing: -3
                            color: "white"

                            wrapMode: Text.WordWrap

                            text: modelData.value

                            Component.onCompleted: {
                                boxValue.lineHeight = 0.8
                            }
                        }

                        Text {
                            id: boxTitle
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: 7

                            font.capitalization: Font.AllUppercase
                            font.pixelSize: 17
                            color: "grey"

                            text: modelData.title
                        }
                    }
                }

                Repeater {
                    id: statisticsBoxRepeater
                    model: weaponStatistics.getStatistics()
                    delegate: statisticsBox
                }

                Component.onCompleted: {
                    flickableMargin.height = flickableMargin.height + height
                }
            }

            Fieldset {
                id: serviceStarFieldset
                title: "Service Stars"
                anchors.top: weaponStatistics.bottom

                visible: weaponData.star.count > 0

                Component.onCompleted: {
                    if (weaponData.star.count > 0) {
                        flickableMargin.height = flickableMargin.height + height
                    }
                }
            }

            Item {
                id: serviceStarWrapper

                width: parent.width
                height: 120
                anchors.top: serviceStarFieldset.bottom

                visible: weaponData.star.count > 0

                Rectangle {
                    anchors.left: parent.left
                    width: (parent.width / 2) - 5
                    height: parent.height
                    color: "gray"
                    opacity: 0.1
                    radius: 9
                }

                Rectangle {
                    id: rightBackground
                    anchors.right: parent.right
                    width: (parent.width / 2) - 5
                    height: parent.height
                    color: "gray"
                    opacity: 0.1
                    radius: 9
                }

                Image {
                    id: serviceStar
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 15

                    opacity: weaponData.star.count > 0 ? 1 : 0.2

                    source: "img/servicestar.png"
                    sourceSize.width: 100
                    sourceSize.height: 100

                    width: 100
                    height: 100
                }

                Rectangle {
                    color: "black"
                    opacity: 0.8
                    radius: 19

                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: serviceStar.right
                    anchors.leftMargin: 15

                    width: 60
                    height: 50

                    Text {
                        text: weaponData.star.count
                        color: "white"
                        font.bold: true
                        font.pixelSize: 30

                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                    }
                }

                Item {
                    width: rightBackground.width
                    height: rightBackground.height
                    anchors.right: parent.right

                    Text {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                        anchors.topMargin: 13

                        font.family: "Nokia Pure"
                        font.pixelSize: 51
                        font.letterSpacing: -3
                        color: "white"

                        wrapMode: Text.WordWrap

                        text: (weaponData.star.needed - weaponData.star.curr)
                    }

                    Text {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 18

                        font.capitalization: Font.AllUppercase
                        font.pixelSize: 21
                        color: "grey"

                        text: "Kills to next"
                    }
                }

                Component.onCompleted: {
                    if (weaponData.star.count > 0) {
                        flickableMargin.height = flickableMargin.height + height
                    }
                }
            }

            Fieldset {
                id: unlocksFieldset
                title: "Unlocks"
                anchors.top: weaponData.star.count > 0 ? serviceStarWrapper.bottom : weaponStatistics.bottom
                visible: (weaponUnlocks.getUnlocks().length > 0)

                Component.onCompleted: {
                    if (weaponUnlocks.getUnlocks().length > 0) {
                        flickableMargin.height = flickableMargin.height + height
                    }
                }
            }

            Grid {
                id: weaponUnlocks
                width: parent.width
                anchors.top: unlocksFieldset.bottom

                visible: (getUnlocks().length > 0)

                columns: 3
                spacing: 10

                function getUnlocks() {
                    return weaponData.unlocks;
                }

                Component {
                    id: unlockCell

                    Item {
                        id: unlockBox
                        width: 149
                        height: 95

                        function getProgressWidth() {
                            if (!modelData.needed) {
                                return 0;

                            } else if (modelData.curr == modelData.needed) {
                                return width - 2 * unlockProgress.anchors.margins;

                            } else {

                                var percent = (modelData.curr * 100) / modelData.needed;
                                var maxWidth = width - 2 * unlockProgress.anchors.margins;
                                var progressWidth = (maxWidth * percent) / 100;

                                return Math.min(maxWidth, progressWidth);
                            }
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                pageStack.push(Qt.resolvedUrl("UnlockPage.qml"), {unlockData: modelData, weaponName: weaponData.name})
                            }
                            onPressed: { cellBackground.opacity = 0.4 }
                            onReleased: { cellBackground.opacity = 0.1 }

                            onContainsMouseChanged: {
                                if (!containsMouse) {
                                    cellBackground.opacity = 0.1
                                }
                            }
                        }

                        Rectangle {
                            id: cellBackground
                            anchors.fill: parent
                            color: "gray"
                            opacity: 0.1
                            radius: 9
                        }

                        Rectangle {
                            id: unlockProgress
                            color: "#E1E9D3"
                            opacity: (modelData.needed && modelData.curr == modelData.needed) ? 0.5 : 0.3
                            radius: 9

                            anchors.margins: 4
                            anchors.left: parent.left
                            anchors.top: parent.top
                            anchors.bottom: parent.bottom

                            width: unlockBox.getProgressWidth()
                        }

                        Image {
                            id: unlockImage
                            anchors.top: parent.top
                            anchors.topMargin: (modelData.id == 'foregrip' || modelData.id == 'bipod') ? 11 : 0
                            anchors.horizontalCenter: parent.horizontalCenter

                            source: "img/small/" + modelData.img
                            sourceSize.width: 127
                            sourceSize.height: 76

                            width: (modelData.id == 'foregrip' || modelData.id == 'bipod') ? 80 : 127
                            height: (modelData.id == 'foregrip' || modelData.id == 'bipod') ? 48 : 76

                            smooth: true
                        }

                        MarqueeText {
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: 7
                            width: parent.width - 10

                            font.capitalization: Font.AllUppercase
                            font.pixelSize: 17
                            color: (!modelData.needed) ? "gray" : "white"

                            text: modelData.name
                        }
                    }
                }

                Repeater {
                    id: unlocksRepeater
                    model: weaponUnlocks.getUnlocks()
                    delegate: unlockCell

                    onItemAdded: {
                        if (!(index % weaponUnlocks.columns)) {
                            var addedHeight = itemAt(index).height;
                            if (index > 0) {
                                addedHeight += weaponUnlocks.spacing;
                            }
                            flickableMargin.height = flickableMargin.height + addedHeight
                        }
                    }
                }
            }
        }
    }

    ScrollDecorator { flickableItem: pageContent }
}
