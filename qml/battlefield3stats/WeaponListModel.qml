import QtQuick 1.1

ListModel {
    id: weaponListModel

    function getWeapons() {
        var data = rootElement.currentSoldier.data;

        // convert hash to array
        var weapons = [];
        if (data && data.stats && data.stats.weapons) {
            for (var key in data.stats.weapons) {
                weapons.push(data.stats.weapons[key]);
            }
        }

        // reorder weapons, weapons with most kills first
        for (var i = 0; i < weapons.length; i++) {
            for (var j = i; j < weapons.length; j++) {
                if (weapons[j].kills > weapons[i].kills) {
                    var tmp = weapons[i];
                    weapons[i] = weapons[j];
                    weapons[j] = tmp;
                }
            }
        }

        return weapons;
    }

    function createListElements() {
        var weapons = getWeapons();
        for (var i = 0; i < weapons.length; i++) {
            weaponListModel.append({"data": weapons[i]});
        }
    }

    Component.onCompleted: {
        createListElements();
    }
}
