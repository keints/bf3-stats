import QtQuick 1.1
import com.nokia.meego 1.0
import "Storage.js" as Storage

Menu {
    id: soldierContextMenu

    property variant soldier: null

    MenuLayout {
        MenuItem {
            text: "View statistics"
            onClicked: {
                rootElement.changeCurrentSoldier(soldier.id);
                commonTools.checkFirstTab();
                pageStack.clear();
                var page = rootElement.getPage("overview");
                page.scrollToTop();
                pageStack.push(page);
            }
        }
        MenuItem {
            text: "Set as default"
            onClicked: {
                Storage.setDefaultSoldier(soldier.id);
                rootElement.refreshSoldiersData();
            }
        }
        MenuItem {
            text: "Refresh data"
            onClicked: {
            }
        }
        MenuItem {
            text: "Delete"
            onClicked: {
                pageStack.currentPage.showDeleteDialog(soldier.id);
            }
        }
    }

    onStatusChanged: {
        if (status == DialogStatus.Closing) {
            soldierContextMenu.destroy();
        }
    }
}
