import QtQuick 1.1
import com.nokia.meego 1.0

MyPage {
    property string name: "soldier"

    property bool showToolbar: false
    tools: showToolbar ? singlePageTools : null

    Rectangle {
        id: searchForm
        color: "black"
        opacity: 0.8
        width: parent.width
        height: (parent.height / 2.1)
        y: showToolbar ? (parent.height - height) : (parent.height - height + 40)
        anchors.horizontalCenter: parent.horizontalCenter

        Text {
            id: label
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 50

            color: "white"

            font.family: "Nokia Pure"
            font.pixelSize: 38
            font.letterSpacing: -1

            text: qsTr("Soldier Search")
        }

        TextField {
            id: name
            width: parent.width / 1.5
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: label.bottom
            anchors.topMargin: 30
            placeholderText: "Soldier name"
            validator: RegExpValidator { regExp: /.{3,}/ }
            onTextChanged: errorHighlight = false
            platformSipAttributes: SipAttributes {
                id: sipAttributes
                actionKeyEnabled: true
                actionKeyHighlighted: true
                actionKeyLabel: "Search"
            }
            focus: true // needed to capture keyboard events
            Keys.onPressed: {
                search.clicked()
            }
        }

        Text {
            text: "Soldier name must be at least 3 chars long"
            color: "red"
            font.pixelSize: 17
            anchors.top: name.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 2
            visible: name.errorHighlight
        }

        SelectionDialog {
            id: platformDialog

            titleText: "Select platform"
            selectedIndex: 0

            model: ListModel {
                ListElement { name: "PC"; value: "pc" }
                ListElement { name: "PS3"; value: "ps3" }
                ListElement { name: "X-Box 360"; value: "xbox360" }
            }
        }

        Button {
            id: platform
            width: parent.width / 1.5
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: name.bottom
            anchors.topMargin: 30
            text:  qsTr("Platform") + ": " + platformDialog.model.get(platformDialog.selectedIndex).name
            platformStyle: ButtonStyle { inverted: false }
            onClicked: {
                platformDialog.open();
            }

            Image {
                source: "img/selectbox/arrows-" + (parent.pressed ? "pressed" : "normal") + ".png"
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                anchors.rightMargin: 20
            }
        }

        Item {
            id: focusDummy
        }

        Button {
            id: search
            width: parent.width / 1.5
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: platform.bottom
            anchors.topMargin: 30
            text:  qsTr("Search")
            onClicked: {
                name.errorHighlight = !name.acceptableInput
                if (name.acceptableInput) {
                    focusDummy.focus = true
                    if (showToolbar) {
                        pageStack.pop()
                    } else {
                        var page = rootElement.getPage("overview");
                        pageStack.replace(page, {}, true)
                    }
                }
            }
        }
    }
}
