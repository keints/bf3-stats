import QtQuick 1.1
import com.nokia.meego 1.0
import "UIConstants.js" as UIConstants
import "Utils.js" as Utils

Item {
    id: weaponListRoot
    width: parent.width
    height: parent.height - UIConstants.ToolbarHeight + 60

    function scrollTop() {
        weaponListView.positionViewAtBeginning();
    }

    Component {
        id: weaponDelegate
        Item {
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.rightMargin: UIConstants.DefaultMargin
            anchors.leftMargin: UIConstants.DefaultMargin
            height: 120

            Rectangle {
                id: rowBackground
                anchors.fill: parent
                radius: 9
                color: "gray"
                opacity: 0.1
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("WeaponPage.qml"), {weaponData: model.data})
                }
                onPressed: { rowBackground.opacity = 0.3 }
                onReleased: { rowBackground.opacity = 0.1 }

                onContainsMouseChanged: {
                    if (!containsMouse) {
                        rowBackground.opacity = 0.1
                    }
                }
            }

            Item {
                id: weaponImageWrapper
                width: 200
                height: parent.height
                anchors.left: parent.left
                anchors.top: parent.top

                Image {
                    anchors.top: parent.top
                    anchors.topMargin: 4
                    anchors.horizontalCenter: parent.horizontalCenter

                    source: "img/small/" + model.data.img
                    // original image size: 512x308
                    sourceSize.width: 155
                    sourceSize.height: 93

                    width: 155
                    height: 93
                }

                Item {
                    id: nameWrapper
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 7
                    width: parent.width

                    height: 20

                    Text {
                        id: weaponName
                        anchors.verticalCenter: parent.verticalCenter
                        x: Math.floor(parent.x + ((parent.width - width) / 2)) // align to center using x attribute

                        font.pixelSize: 17
                        color: "white"
                        text: model.data.name
                    }

                    Image {
                        id: kitLogo
                        anchors.right: weaponName.left
                        anchors.rightMargin: 5
                        anchors.verticalCenter: parent.verticalCenter

                        visible: (model.data.kit != "general")

                        source: model.data.kit != "general" ? "img/small/kit/" + model.data.kit + ".png" : ""
                        sourceSize.width: 18
                        sourceSize.height: 18

                        width: 18
                        height: 18

                        Component.onCompleted: {
                            if (model.data.kit != "general") {
                                weaponName.x += Math.floor((width + anchors.rightMargin) / 2)
                            }
                        }
                    }
                }
            }

            Item {
                id: starWrapper
                width: 60
                height: parent.height
                anchors.left: weaponImageWrapper.right
                anchors.top: parent.top

                Image {
                    id: serviceStar
                    anchors.top: parent.top
                    anchors.topMargin: 12
                    anchors.horizontalCenter: parent.horizontalCenter

                    opacity: model.data.star.count > 0 ? 1 : 0.2

                    source: "img/servicestar_small.png"
                    sourceSize.width: 60
                    sourceSize.height: 60

                    width: 60
                    height: 60

                    fillMode: Image.PreserveAspectFit

                    Behavior on width {
                        NumberAnimation { duration: 700; easing.type: Easing.OutBack }
                    }

                    onVisibleChanged: {
                        if (weaponListRoot.visible) {
                            width = 60
                        } else if (model.data.star.count > 0) {
                            width = 0
                        }
                    }
                }

                Rectangle {
                    color: "white"
                    opacity: 0.8
                    radius: 13

                    visible: model.data.star.count > 0

                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: serviceStar.bottom
                    anchors.topMargin: 5

                    width: 35
                    height: 28

                    Text {
                        text: model.data.star.count
                        color: "black"
                        font.bold: true
                        font.pixelSize: 18

                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                    }
                }
            }

            Item {
                id: accuracyWrapper
                width: 95
                height: parent.height
                anchors.left: starWrapper.right
                anchors.top: parent.top

                Text {

                    function getAccuracy() {
                        return Utils.getAccuracy(model.data.shots, model.data.hits);
                    }

                    id: accuracyValue
                    width: parent.width
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: parent.top
                    anchors.topMargin: 3
                    anchors.bottom: accuracyTitle.top
                    anchors.bottomMargin: 3

                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    clip: true

                    font.family: "Nokia Pure"
                    font.pixelSize: 24
                    font.letterSpacing: -3
                    color: "white"

                    wrapMode: Text.WordWrap

                    text: getAccuracy()

                    Component.onCompleted: {
                        accuracyValue.lineHeight = 0.8
                    }
                }

                Text {
                    id: accuracyTitle
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 10

                    font.capitalization: Font.AllUppercase
                    font.pixelSize: 17
                    color: "grey"

                    text: "Accuracy"
                }
            }

            Item {
                id: killsWrapper
                width: 110
                height: parent.height
                anchors.left: accuracyWrapper.right
                anchors.top: parent.top

                Text {
                    id: killsValue
                    width: parent.width
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: parent.top
                    anchors.topMargin: 3
                    anchors.bottom: killsTitle.top
                    anchors.bottomMargin: 3

                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    clip: true

                    font.family: "Nokia Pure"
                    font.pixelSize: 24
                    font.letterSpacing: -3
                    color: "white"

                    wrapMode: Text.WordWrap

                    text: model.data.kills

                    Component.onCompleted: {
                        killsValue.lineHeight = 0.8
                    }
                }

                Text {
                    id: killsTitle
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 10

                    font.capitalization: Font.AllUppercase
                    font.pixelSize: 17
                    color: "grey"

                    text: "Kills"
                }
            }
        }
    }

    ListView {
        id: weaponListView
        anchors.fill: parent
        model: WeaponListModel {}
        delegate: weaponDelegate
        spacing: 8
        snapMode: ListView.SnapToItem
        cacheBuffer: 1800 // cache 15 items both sides for smooth scrolling
        header: Fieldset {
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.rightMargin: UIConstants.DefaultMargin
            anchors.leftMargin: UIConstants.DefaultMargin
            title: "Weapons ordered by kills"
        }
    }

    ScrollDecorator { flickableItem: weaponListView }
}
