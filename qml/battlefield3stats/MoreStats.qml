import QtQuick 1.0
import "Utils.js" as Utils

Item {
    id: moreStatsRoot

    property int kills: 0
    property int deaths: 0
    property real kill_assists: 0
    property int vehicles_destroyed: 0
    property real vehicles_destroyed_assists: 0
    property int headshots: 0
    property real longest_headshot: 0
    property int kill_streak_bonus: 0
    property int squad_score_bonus: 0
    property real repairs: 0
    property int revives: 0
    property real heals: 0
    property int resupplies: 0

    width: parent.width

    Grid {
        id: statsLayout

        width: parent.width

        function getStatistics() {
            return [
                { title: "Kills", value: Utils.beautifyNumber(Math.floor(parent.kills)) },
                { title: "Deaths", value: Utils.beautifyNumber(Math.floor(parent.deaths)) },
                { title: "Kill Assists", value: Utils.beautifyNumber(Math.floor(parent.kill_assists)) },
                { title: "Vehicles Destroyed", value: Utils.beautifyNumber(Math.floor(parent.vehicles_destroyed)) },
                { title: "Vehicles Destroyed Assists", value: Utils.beautifyNumber(Math.floor(parent.vehicles_destroyed_assists)) },
                { title: "Headshots", value: Utils.beautifyNumber(Math.floor(parent.headshots)) },
                { title: "Longest Headshot", value: Utils.beautifyNumber(parent.longest_headshot) + " m" },
                { title: "Kill Streak Bonus", value: Utils.beautifyNumber(Math.floor(parent.kill_streak_bonus)) },
                { title: "Squad Score Bonus", value: Utils.beautifyNumber(Math.floor(parent.squad_score_bonus)) + " XP" },
                { title: "Repairs", value: Utils.beautifyNumber(Math.floor(parent.repairs)) },
                { title: "Revives", value: Utils.beautifyNumber(Math.floor(parent.revives)) },
                { title: "Heals", value: Utils.beautifyNumber(Math.floor(parent.heals)) },
                { title: "Resupplies", value: Utils.beautifyNumber(Math.floor(parent.resupplies)) }
            ];
        }

        columns: 1
        spacing: 10

        Component {
            id: statsRow

            DataRow {
                title: modelData.title
                value: modelData.value
            }
        }

        Repeater {
            id: statsRepeater
            model: statsLayout.getStatistics()
            delegate: statsRow
        }
    }
}
