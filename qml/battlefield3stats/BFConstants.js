var serviceStarThresholds = {
    'assault': 220000,
    'engineer': 145000,
    'support':  170000,
    'recon': 195000
};

var weaponUnlocks = {
    'ACOG (4x)': "The Advanced Combat Optic Gunsight is a medium-speed, medium-range scope that magnifies at 4x. A ballistic reticule makes gauging bullet drop at longer ranges easier.",
    'Ballistic (12x)': "The Ballistic Scope's 12x magnification allows for the greatest accuracy for extremely long-range engagements. Ballistic Scope reflections can reveal your position when aimed at enemies.",
    'Holographic (HOLO)': "A Holographic Sight provides a fast, open sighting picture for mid- to close-range engagements. The reticule features a circle dot configuration.",
    'IRNV (IR 1x)': "An IR Enhanced Night Vision Scope for low- and no-light situations, the IRNV scope can be steadied by holding the sprint button when aimed, or by using a Bipod.",
    'KOBRA (RDS)': "Russian Red Dot Sight used by military and police organizations on AK-style mounts, adapted to fit standard accessory rails; features a single red dot reticule.",
    'M145 (3.4x)': "Originally developed for the Canadian Army, the M145 is commonly mounted on the M240 and M249 with a 3.4x zoom. A ballistic reticule helps with longer range engagements.",
    'PK-A (3.4x)': "Common Russian mid-range scope for AK and SAIGA-style receivers with a chevron reticule and 3.4x zoom; faster aiming than the PSO-1 with less zoom.",
    'PKA-S (HOLO)': "Russian advanced Holographic Sight featuring a circle and dot reticule for fast sighting. Great for mid- to close-range engagements with an open sight picture.",
    'PKS-07 (7x)': "A Russian high magnification 7x scope with ballistic reticule. The PKS-07 can be steadied by holding the sprint button when aimed, or by using a Bipod. The PKS-07 scope reflections can reveal your position when aimed at enemies.",
    'PSO-1 (4x)': "Standard-issue Russian medium-speed, medium-range scope with a 4x magnification originally developed for the SVD; adapted to fit on standard rails with a ballistic reticule.",
    'Reflex (RDS)': "American made Red Dot Sight for extremely fast and clear target acquisition in close quarters. The single red dot makes hitting targets easier.",
    'Rifle Scope (6x)': "A high-magnification 6x scope available for most rifles, the Rifle Scope can be steadied by holding the sprint button when aimed, or by using a Bipod, but can reveal your position when aimed at enemies.",
    'Rifle Scope (8x)': "A high-magnification 8x scope available for sniper rifles. The Rifle Scope can be steadied by holding the sprint button when aimed, or by using a Bipod, but can reveal your position when aimed at enemies.",
    "Heavy Barrel": "A Heavy Barrel gives greater accuracy for aimed fire, but increases the total vertical muzzle climb due to the use of heavy match ammunition. Match Rounds do not increase the overall damage of the weapon.",
    "Foregrip": "A Foregrip decreases the amount of horizontal muzzle drift when firing a weapon, giving better control over long bursts. Vertical muzzle climb is not affected.",
    "Tactical Light": "A Tactical Light lights up the dark environments with white light. In close quarters the Tactical Light can also disorient and temporarily blind others, but can also reveal your position.",
    "Bipod": "Bipods allow the shooter to Support their gun on flat horizontal surfaces, or when prone, to gain an increased accuracy and reduced recoil. By default, Aiming will enter Supported Shooting.",
    "Suppressor": "A Sound Suppressor significantly reduces the sound signature and eliminates muzzle flash. However, the cold-loaded ammunition used by Suppressed weapons travels slower and does less damage at long range.",
    "Laser Sight": "The Laser Sight adds a visible laser aiming dot to the weapon which increases accuracy from the hip. The Laser Sight can dazzle enemies when turned on, but also reveals your position.",
    "Flash Supp.": "The Flash Suppressor hides muzzle flash but has no effect on sound. Flash Suppressors decrease aimed and automatic accuracy.",
    "Extended Mag": "Extended Magazines increase the total amount of bullets in each magazine, giving more shots before you need to reload.",
    "12G Flechette": "Flechette rounds have increased penetration ability but a reduced damage potential compared to buckshot.",
    "12G Frag": "An advanced explosive round, FRAG rounds provides enhanced suppression but lacks accuracy or stopping power.",
    "12G Slug": "SABOT Slug rounds fire a single fin stabilized projectile for longer range than standard shot. Slugs are not accurate at extreme ranges, and have less stopping power in CQB than standard buckshot."
};

