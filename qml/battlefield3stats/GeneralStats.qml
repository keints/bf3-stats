import QtQuick 1.0
import com.nokia.meego 1.0
import "Utils.js" as Utils

Item {
    id: generalStatsRoot

    property int time: null
    property int score: null
    property int kills: null
    property int deaths: null
    property int wins: null
    property int losses: null
    property int shots: null
    property int hits: null

    property real infobox_opacity: 0.1
    property real infobox_margin: 10
    property int infobox_radius: 9
    property color infobox_color: "gray"

    width: parent.width
    height: 200

    Grid {
        id: infoboxLayout

        columns: 3
        rows: 2
        spacing: 10

        // calculate infoblock width
        function getBlockWidth() {
            return (parent.width - ((spacing * (columns - 1)))) / columns;
        }

        // calculate infoblock height
        function getBlockHeight() {
            return (parent.height - ((spacing * (rows - 1)))) / rows;
        }

        Component {
            id: infoBox

            Item {
                property string title: ""
                property string value: ""
                property int value_font_size: 35

                width: infoboxLayout.getBlockWidth()
                height: infoboxLayout.getBlockHeight()

                Rectangle {
                    anchors.fill: parent
                    color: generalStatsRoot.infobox_color
                    opacity: generalStatsRoot.infobox_opacity
                    radius: generalStatsRoot.infobox_radius
                }

                Text {
                    id: boxValue
                    width: parent.width
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: parent.top
                    anchors.topMargin: 3
                    anchors.bottom: boxTitle.top
                    anchors.bottomMargin: 3

                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    clip: true

                    font.family: "Nokia Pure"
                    font.pixelSize: parent.value_font_size
                    font.letterSpacing: -3
                    color: "white"

                    wrapMode: Text.WordWrap

                    text: parent.value

                    Component.onCompleted: {
                        boxValue.lineHeight = 0.8
                    }
                }

                Text {
                    id: boxTitle
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 7

                    font.capitalization: Font.AllUppercase
                    font.pixelSize: 17
                    color: "grey"

                    text: parent.title
                }
            }
        }

        Loader {
            sourceComponent: infoBox

            Component.onCompleted: {
                item.value_font_size = 28
                item.title = "Time played"

                // "generalStatsRoot.time" is in seconds
                item.value = Utils.humanTime(generalStatsRoot.time);
            }
        }
        Loader {
            sourceComponent: infoBox

            Component.onCompleted: {
                item.title = "Score / Min"

                // "time" is in seconds
                var minutes = Math.round(generalStatsRoot.time / 60);
                var score = generalStatsRoot.score;
                item.value = Math.round(score / minutes);
            }
        }
        Loader {
            sourceComponent: infoBox

            Component.onCompleted: {
                item.value_font_size = 27
                item.title = "Kills"
                item.value = Utils.beautifyNumber(generalStatsRoot.kills)
            }
        }

        Loader {
            sourceComponent: infoBox

            Component.onCompleted: {
                item.title = "K/D ratio"

                var value = (generalStatsRoot.kills / generalStatsRoot.deaths) + "";
                var commaPosition = value.indexOf(".");
                if (commaPosition != -1) {
                    value = value.substr(0, commaPosition + 3);
                }

                item.value = value
            }
        }
        Loader {
            sourceComponent: infoBox

            Component.onCompleted: {
                item.title = "Accuracy"
                item.value =  Utils.getAccuracy(generalStatsRoot.shots, generalStatsRoot.hits);
            }
        }
        Loader {
            sourceComponent: infoBox

            Component.onCompleted: {
                item.title = "W/L ratio"

                var value = (generalStatsRoot.wins / generalStatsRoot.losses) + "";
                var commaPosition = value.indexOf(".");
                if (commaPosition != -1) {
                    value = value.substr(0, commaPosition + 3);
                }

                item.value = value
            }
        }
    }
}
