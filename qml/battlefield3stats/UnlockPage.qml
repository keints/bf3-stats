import QtQuick 1.1
import com.nokia.meego 1.0
import "Utils.js" as Utils
import "UIConstants.js" as UIConstants
import "BFConstants.js" as BFConstants

MyTabPage {
    id: unlockPage
    tools: singlePageTools

    property variant unlockData: null
    property string weaponName: null

    SoldierHeaderBar {
        id: header
        z: 100
    }

    Flickable {
        id: pageContent
        width: parent.width
        height: parent.height - UIConstants.ToolbarHeight

        anchors.top: header.bottom

        contentWidth: parent.width
        contentHeight: 0

        Item {
            id: flickableMargin

            onHeightChanged: {
                pageContent.contentHeight = height
            }

            anchors.top: parent.top
            anchors.left: parent.left
            anchors.leftMargin: UIConstants.DefaultMargin
            anchors.right: parent.right
            anchors.rightMargin: UIConstants.DefaultMargin

            height: unlockImageFieldset.height + unlockImageBackground.height + nameDescriptionFieldset.height + unlockName.height + 60

            Fieldset {
                id: unlockImageFieldset
                title: "Image"
            }

            Rectangle {
                id: unlockImageBackground
                color: "gray"
                opacity: 0.1
                width: parent.width
                height: 270
                radius: 9
                anchors.top: unlockImageFieldset.bottom
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Image {
                id: unlockImage
                anchors.verticalCenter: unlockImageBackground.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter

                source: "img/" + unlockPage.unlockData.img
                sourceSize.width: 426
                sourceSize.height: 256

                width: 426
                height: 256
            }

            Fieldset {
                id: nameDescriptionFieldset
                title: "Name & Description"
                anchors.top: unlockImageBackground.bottom
            }

            Text {
                id: unlockName
                anchors.top: nameDescriptionFieldset.bottom
                anchors.left: parent.left

                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                clip: true

                font.family: "Nokia Pure"
                font.pixelSize: 30
                font.letterSpacing: -1
                color: "white"

                wrapMode: Text.WordWrap

                text: unlockData.name
            }

            Text {
                id: unlockDesc
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: unlockName.bottom

                visible: BFConstants.weaponUnlocks[unlockData.name] ? true : false

                horizontalAlignment: Text.AlignJustify
                verticalAlignment: Text.AlignVCenter
                clip: true

                font.family: "Nokia Pure"
                font.pixelSize: 20
                font.letterSpacing: -1
                color: "gray"

                wrapMode: Text.WordWrap

                text: BFConstants.weaponUnlocks[unlockData.name] ? BFConstants.weaponUnlocks[unlockData.name] : ''

                Component.onCompleted: {
                    if (BFConstants.weaponUnlocks[unlockData.name]) {
                        flickableMargin.height = flickableMargin.height + height
                    }
                }
            }

            Fieldset {
                id: unlockProgressFieldset
                title: "Unlocked for " + weaponName
                anchors.top: unlockDesc.bottom
            }

            Item {
                anchors.top: unlockProgressFieldset.bottom
                width: parent.width
                height: 50

                function getTitle() {
                    if (!unlockData.needed) {
                        return "Not available";
                    } else if (unlockData.curr == unlockData.needed) {
                        return "Allready unlocked";
                    } else {
                        return "Kills needed";
                    }
                }

                function getValue() {
                    if (!unlockData.needed) {
                        return "";
                    } else if (unlockData.curr == unlockData.needed) {
                        return "";
                    } else {
                        return Utils.beautifyNumber(unlockData.curr) + " / " + Utils.beautifyNumber(unlockData.needed);
                    }
                }

                Rectangle {
                    anchors.fill: parent
                    radius: 9
                    color: "gray"
                    opacity: 0.1
                }

                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 12

                    font.capitalization: Font.AllUppercase
                    font.pixelSize: 20
                    color: "grey"

                    text: parent.getTitle()
                }

                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    anchors.rightMargin: 12

                    font.capitalization: Font.AllUppercase
                    font.pixelSize: 20
                    color: "white"

                    text: parent.getValue()
                }
            }

        }
    }

    ScrollDecorator { flickableItem: pageContent }
}
