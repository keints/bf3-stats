import QtQuick 1.1
import com.nokia.meego 1.0
import "UIConstants.js" as UIConstants
import "Utils.js" as Utils

MyPage {
    id: soldierListPageRoot
    tools: soldierListPageTools

    property variant selectedSoldiers: []

    function updateSelectedSoldiers() {
        var soldierIds = [];
        for (var i = 0; i < soldierRowRepeater.count; i++) {
            var selectedId = soldierRowRepeater.itemAt(i).getSelectedId();
            if (selectedId !== null) {
                soldierIds.push(selectedId);
            }
        }
        selectedSoldiers = soldierIds;
    }

    function showDeleteDialog(soldierId) {
        var soldierIds = soldierId ? [soldierId] : soldierListPageRoot.selectedSoldiers;
        var confirmComponent = Qt.createComponent("SoldierDeleteDialog.qml");
        var confirmObj = confirmComponent.createObject(pageStack.currentPage, {soldierIds: soldierIds});
        confirmObj.open();
    }

    function cancelDelete() {
        cancelDeleteButton.clicked();
    }

    ToolBarLayout {
        visible: false
        id: soldierListPageTools

        ToolIcon {
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            iconId: theme.inverted ? "icon-m-toolbar-back-white" : "icon-m-toolbar-back";
            onClicked: pageStack.pop();
            visible: soldierListPageRoot.state == ""
        }

        ToolIcon {
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            iconId: "toolbar-add";
            onClicked: {
                var page = rootElement.getPage("soldier");
                pageStack.push(page, {showToolbar: true});
            }
            visible: soldierListPageRoot.state == ""
        }

        ToolIcon {
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            iconId: theme.inverted ? "icon-m-toolbar-view-menu-white" : "icon-m-toolbar-view-menu";
            onClicked: (manageSoldiersMenu.status == DialogStatus.Closed) ? manageSoldiersMenu.open() : manageSoldiersMenu.close()
            visible: soldierListPageRoot.state == ""
        }

        Item {
            width: 290
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            visible: soldierListPageRoot.state == "delete"

            Button {
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                width: parent.width / 2.2
                text: "Delete"
                enabled: (soldierListPageRoot.selectedSoldiers.length > 0)
                onClicked: {
                    soldierListPageRoot.showDeleteDialog();
                }
            }
            Button {
                id: cancelDeleteButton
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                width: parent.width / 2.2
                text: "Cancel"
                onClicked: {
                    soldierListPageRoot.state = ""
                    for (var i = 0; i < soldierRowRepeater.count; i++) {
                        // deselect checkboxes on cancel
                        soldierRowRepeater.itemAt(i).clearCheckbox();
                    }
                }
            }
        }
    }

    Menu {
        id: manageSoldiersMenu
        MenuLayout {
            MenuItem {
                text: "Delete soldiers"
                onClicked: {
                    soldierListPageRoot.state = "delete"
                }
            }
        }
    }

    HeaderBar {
        id: header
        title: "Manage Soldiers"
        z: 100
    }

    Flickable {
        id: pageContent
        width: parent.width
        height: parent.height - UIConstants.ToolbarHeight

        anchors.top: header.bottom

        contentWidth: parent.width
        contentHeight: 0

        Item {
            id: flickableMargin
            anchors.fill: parent
            height: 15

            onHeightChanged: {
                pageContent.contentHeight = height;
            }

            Column {
                id: soldierList
                width: parent.width

                function getSoldiers() {
                    return rootElement.soldiers;
                }

                spacing: 0

                Component {
                    id: soldierRow

                    Item {
                        width: soldierListPageRoot.width
                        height: 95

                        function clearCheckbox() {
                            if (deleteCheckbox.checked) {
                                deleteCheckbox.checked = false;
                            }
                        }

                        function getSelectedId() {
                            if (deleteCheckbox.checked) {
                                return modelData.id;
                            }
                            return null;
                        }

                        Rectangle {
                            id: rowBackground
                            width: parent.width
                            height: parent.height
                            anchors.bottom: parent.bottom
                            color: "black"
                            opacity: getOpacity()

                            function getOpacity() {
                                return (index % 2) ? 0.4 : 0;
                            }
                        }

                        Image {
                            id: rowHighlight
                            anchors.top: parent.top
                            anchors.left: parent.left

                            source: "img/row_highlight.png"
                            sourceSize.width: 5
                            sourceSize.height: 40
                            opacity: 0

                            width: parent.width
                            height: 40

                            smooth: true
                        }

                        Item {
                            id: rowWrapper
                            anchors.fill: parent

                            Item {
                                id: soldierDummyWrapper
                                anchors.left: parent.left
                                anchors.leftMargin: 13
                                width: 65
                                height: parent.height

                                Image {
                                    id: soldierDummy
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.horizontalCenter: parent.horizontalCenter

                                    source: "img/soldier_dummy.png"
                                    sourceSize.width: 65
                                    sourceSize.height: 65

                                    width: 65
                                    height: 65

                                    Behavior on width {
                                        NumberAnimation { duration: 90; easing.type: Easing.OutCubic }
                                    }

                                    fillMode: Image.PreserveAspectFit
                                    smooth: true
                                }

                                Image {
                                    id: checkedIcon
                                    visible: (modelData.is_default == '1')
                                    anchors.top: soldierDummy.top
                                    anchors.topMargin: -10
                                    anchors.right: soldierDummy.right
                                    anchors.rightMargin: -10

                                    source: "img/checked_icon.png"
                                    sourceSize.width: 35
                                    sourceSize.height: 35

                                    width: 35
                                    height: 35

                                    smooth: true
                                }
                            }

                            Item {
                                id: textWrapper
                                height: parent.height
                                width: 290
                                anchors.left: soldierDummyWrapper.right
                                anchors.leftMargin: 18

                                MarqueeText {
                                    id: soldierName
                                    width: parent.width
                                    text: modelData.name

                                    font.family: "Nokia Pure"
                                    font.pixelSize: 33
                                    font.letterSpacing: -1
                                    textAlign: "left"
                                    color: "white"

                                    anchors.top: parent.top
                                    anchors.topMargin: 11
                                }

                                Text {
                                    function getText() {
                                        var text = "<style type='text/css'>b { color: 'orange'; }</style><b>";
                                        if (modelData.plat == "360") {
                                            text += "X-Box 360";
                                        } else if (modelData.plat == "ps3") {
                                            text += "PS3";
                                        } else {
                                            text += "PC";
                                        }
                                        text += "</b>";
                                        if (modelData.date_update) {
                                            text += ", updated at " + Utils.getDateString(modelData.date_update);
                                        } else {
                                            text += ", data missing"
                                        }

                                        return text;
                                    }

                                    id: soldierDetails
                                    color: "gray"
                                    anchors.left: parent.left
                                    anchors.top: soldierName.bottom
                                    anchors.topMargin: 2
                                    textFormat: Text.RichText
                                    font.family: "Nokia Pure"
                                    font.pixelSize: 19

                                    text: getText()
                                }
                            }

                            CheckBox {
                                id: deleteCheckbox
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.right: parent.right
                                anchors.rightMargin: 13
                                visible: soldierListPageRoot.state == "delete"

                                onCheckedChanged: {
                                    soldierListPageRoot.updateSelectedSoldiers();
                                }
                            }

                            MouseArea {
                                anchors.fill: parent

                                onClicked: {
                                    if (soldierListPageRoot.state == "delete") {
                                        deleteCheckbox.checked = !deleteCheckbox.checked;
                                    } else {
                                        var menuComponent = Qt.createComponent("SoldierContextMenu.qml");
                                        var menuObj = menuComponent.createObject(pageStack.currentPage, {soldier: modelData});
                                        menuObj.open();
                                    }
                                }
                                onPressed: {
                                    rowBackground.opacity += 0.2
                                    rowHighlight.opacity = 0.5
                                    soldierDummy.width = 60
                                    soldierName.anchors.topMargin = 12
                                    soldierDetails.anchors.topMargin = 0
                                }
                                onReleased: {
                                    rowBackground.opacity = rowBackground.getOpacity()
                                    rowHighlight.opacity = 0
                                    soldierDummy.width = 65
                                    soldierName.anchors.topMargin = 11
                                    soldierDetails.anchors.topMargin = 2
                                }

                                onContainsMouseChanged: {
                                    if (!containsMouse) {
                                        rowBackground.opacity = rowBackground.getOpacity()
                                        rowHighlight.opacity = 0
                                        soldierDummy.width = 65
                                        soldierName.anchors.topMargin = 11
                                        soldierDetails.anchors.topMargin = 2
                                    }
                                }
                            }
                        }
                    }
                }

                Repeater {
                    id: soldierRowRepeater
                    model: soldierList.getSoldiers()
                    delegate: soldierRow

                    onItemAdded: {
                        var addedHeight = itemAt(index).height;
                        if (index > 0) {
                            addedHeight += parent.spacing;
                        }
                        flickableMargin.height = flickableMargin.height + addedHeight
                    }
                }
            }
        }
    }

    ScrollDecorator { flickableItem: pageContent }

    states: [
        State {
            name: ""
        },
        State {
            name: "delete"
        }
    ]
}
