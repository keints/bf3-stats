import QtQuick 1.1
import com.nokia.meego 1.0

Page {
    orientationLock: PageOrientation.LockPortrait
    anchors.topMargin: rootElement.showStatusBar ? rootElement.statusbarMargin : 0
    anchors.fill: parent
}
