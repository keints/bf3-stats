import QtQuick 1.1
import com.nokia.meego 1.0
import "UIConstants.js" as UIConstants

MyPage {
    id: ribbonsPage
    tools: singlePageTools

    property variant ribbonsData: [];

    SoldierHeaderBar {
        id: header
        z: 100
    }

    Flickable {
        id: pageContent

        anchors.top: header.bottom

        width: parent.width
        height: parent.height - UIConstants.ToolbarHeight

        contentWidth: parent.width
        contentHeight: flickableMargin.height

        Item {
            id: flickableMargin
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.rightMargin: UIConstants.DefaultMargin
            anchors.leftMargin: UIConstants.DefaultMargin
            height: ribbonsFieldset.height + 30

            onHeightChanged: {
                pageContent.contentHeight = height
            }

            Fieldset {
                id: ribbonsFieldset
                title: "Ribbons"
            }

            Grid {
                id: ribbonsLayout

                anchors.top: ribbonsFieldset.bottom
                anchors.topMargin: 5

                columns: 2
                spacing: 15

                // calculate infoblock width
                function getBlockWidth() {
                    return (parent.width - ((spacing * (columns - 1)))) / columns;
                }

                function getRibbons() {
                    return ribbonsPage.ribbonsData;
                }

                Component {
                    id: ribbonComp

                    Item {
                        width: ribbonsLayout.getBlockWidth()
                        height: 70

                        Image {
                            id: ribbonImage
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.horizontalCenter: parent.horizontalCenter

                            opacity: modelData.count > 0 ? 1 : 0.3

                            source: "img/small/" + modelData.img_large
                            sourceSize.width: 226
                            sourceSize.height: 84

                            width: 226
                            height: 84
                        }

                        Rectangle {
                            id: countBackground
                            y: ribbonImage.y + 12
                            x: ribbonImage.x + ribbonImage.width - width - 8
                            width: 0
                            height: 25
                            color: "#545454"

                            opacity: 0.8
                            visible: modelData.count > 1 ? true : false
                        }

                        Text {
                            anchors.verticalCenter: countBackground.verticalCenter
                            anchors.horizontalCenter: countBackground.horizontalCenter
                            color: "white"
                            font.pixelSize: 17

                            visible: modelData.count > 1 ? true : false

                            text: "x" + modelData.count

                            Component.onCompleted: {
                                countBackground.width = width + 15
                            }
                        }

                        Rectangle {
                            id: ribbonBackground
                            anchors.fill: parent
                            color: "gray"
                            opacity: 0
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                var ribbonDialogComponent = Qt.createComponent("RibbonDialog.qml");
                                var ribbonDialogObj = ribbonDialogComponent.createObject(ribbonsPage, {ribbon: modelData});
                                ribbonDialogObj.open();
                            }
                            onPressed: { ribbonBackground.opacity = 0.3 }
                            onReleased: { ribbonBackground.opacity = 0 }

                            onContainsMouseChanged: {
                                if (!containsMouse) {
                                    ribbonBackground.opacity = 0
                                }
                            }
                        }
                    }
                }

                Repeater {
                    id: ribbonRepeater
                    model: ribbonsLayout.getRibbons()
                    delegate: ribbonComp

                    onItemAdded: {
                        if (!(index % ribbonsLayout.columns)) {
                            var addedHeight = itemAt(index).height;
                            if (index > 0) {
                                addedHeight += ribbonsLayout.spacing;
                            }
                            flickableMargin.height = flickableMargin.height + addedHeight
                        }
                    }
                }
            }

        }
    }

    ScrollDecorator { flickableItem: pageContent }
}
