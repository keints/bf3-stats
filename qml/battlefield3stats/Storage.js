Qt.include("TestData.js")

function getDatabase() {
    return openDatabaseSync("taskuvara-battlefield3stats", "1.0", "BattleStats database", 5242880); // 5 MB in size
}

function initialize() {
    var db = getDatabase();
    db.transaction(
        function(tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS soldiers (id INTEGER PRIMARY KEY, current INTEGER DEAFULT "0", is_default INTEGER DEAFULT "0", name VARCHAR(100), platform VARCHAR(10), date_update INTEGER, date_check INTEGER, status VARCHAR(10), data TEXT)');
    });
}

function insertSoldier(soldier) {
    var db = getDatabase();
    db.transaction(function(tx) {
        tx.executeSql('INSERT OR REPLACE INTO soldiers (current, is_default, name, platform, date_update, date_check, status, data) VALUES (?,?,?,?,?,?,?,?)',
               [0, 0, soldier.name, 'ps3', 1321133200, 1320217388, 'ok', 'data']);
    });
}

function insertData() {
    var db = getDatabase();
    /*db.transaction(
        function(tx) {
            tx.executeSql('DELETE FROM soldiers');
    });*/
    db.transaction(
        function(tx) {
            var rs = tx.executeSql('SELECT * FROM soldiers');
            if (rs.rows.length == 0) {
                var testData = JSON.stringify(getData());
                tx.executeSql('INSERT OR REPLACE INTO soldiers (current, is_default, name, platform, date_update, date_check, status, data) VALUES (?,?,?,?,?,?,?,?)',
                        [0, 0, 'Vessse', 'ps3', 1321133200, 1320217388, 'ok', testData]);
                tx.executeSql('INSERT OR REPLACE INTO soldiers (current, is_default, name, platform, date_update, date_check, status, data) VALUES (?,?,?,?,?,?,?,?)',
                        [1, 1, 'keints', 'ps3', 1321133200, 1320217388, 'ok', testData]);
                tx.executeSql('INSERT OR REPLACE INTO soldiers (current, is_default, name, platform, date_update, date_check, status, data) VALUES (?,?,?,?,?,?,?,?)',
                        [0, 0,'aifu22', '360', 1321133200, 1320217388, 'ok', testData]);
                tx.executeSql('INSERT OR REPLACE INTO soldiers (current, is_default, name, platform, date_update, date_check, status, data) VALUES (?,?,?,?,?,?,?,?)',
                        [0, 0,'Anythinghappened3', 'pc', 1321133200, 1320217388, 'ok', testData]);
            }
    });
}

function getSoldiersList() {
    var db = getDatabase();
    var soldiers = [];
    db.transaction(function(tx) {
        var rs = tx.executeSql('SELECT id, name, platform, date_update, current, is_default FROM soldiers');
        if (rs.rows.length > 0) {
            for (var i = 0; i < rs.rows.length; i++) {
                soldiers.push({
                    'id': rs.rows.item(i).id,
                    'name': rs.rows.item(i).name,
                    'platform': rs.rows.item(i).platform,
                    'date_update': rs.rows.item(i).date_update,
                    'current': rs.rows.item(i).current,
                    'is_default': rs.rows.item(i).is_default
                });
            }
        }
    });
    return soldiers;
}

function getCurrentSoldier(forceDefault) {
    var db = getDatabase();

    if (forceDefault) {
        db.transaction(function(tx) {
            var rs = tx.executeSql('SELECT id FROM soldiers WHERE is_default = "1"');
            if (rs.rows.length > 0) {
                // force current soldier to default one
                tx.executeSql('UPDATE soldiers SET current = "0"');
                tx.executeSql('UPDATE soldiers SET current = "1" WHERE is_default = "1"');
            } else {
                // default soldier is missing for some reason, set default same as current
                tx.executeSql('UPDATE soldiers SET is_default = "1" WHERE current = "1"');
            }
        });
    }

    var soldier = null;
    db.transaction(function(tx) {
        var rs = tx.executeSql('SELECT * FROM soldiers WHERE current = "1"');
        if (rs.rows.length > 0) {
            soldier = rs.rows.item(0);
            soldier.data = JSON.parse(soldier.data); // convert JSON string to hash
        }
    });
    return soldier;
}

function setCurrentSoldier(soldierId) {
    var db = getDatabase();
    db.transaction(function(tx) {
        tx.executeSql('UPDATE soldiers SET current = "0"');
        tx.executeSql('UPDATE soldiers SET current = "1" WHERE id = ?', [soldierId]);
    });
    return getCurrentSoldier();
}

function setDefaultSoldier(soldierId) {
    var db = getDatabase();
    db.transaction(function(tx) {
        tx.executeSql('UPDATE soldiers SET is_default = "0"');
        tx.executeSql('UPDATE soldiers SET is_default = "1" WHERE id = ?', [soldierId]);
    });
}

function deleteSoldier(soldierId) {
    var db = getDatabase();
    db.transaction(function(tx) {
        var rs = tx.executeSql('SELECT * FROM soldiers WHERE id = ?', [soldierId]);
        if (rs.rows.length > 0) {
            var isCurrent = (rs.rows.item(0).current == '1');
            var isDefault = (rs.rows.item(0).is_default == '1');

            tx.executeSql('DELETE FROM soldiers WHERE id = ?', [soldierId]);

            // update 'current' and 'is_default' flags if needed
            var rs2 = tx.executeSql('SELECT id FROM soldiers ORDER BY id LIMIT 1');
            if (rs2.rows.length > 0) {
                if (isCurrent) {
                    tx.executeSql('UPDATE soldiers SET current = "1" WHERE id = ?', [rs2.rows.item(0).id]);
                }
                if (isDefault) {
                    tx.executeSql('UPDATE soldiers SET is_default = "1" WHERE id = ?', [rs2.rows.item(0).id]);
                }
            }
        }
    });
}

function savedSoldiersExist() {
    var db = getDatabase();
    var result = false;
    db.transaction(function(tx) {
        var rs = tx.executeSql('SELECT id FROM soldiers LIMIT 1');
        result = (rs.rows.length > 0);
    });
    return result;
}
