import QtQuick 1.0
import "BFConstants.js" as BFConstants

Item {
    id: kitOverviewRoot

    property int score_assault: 0
    property int score_engineer: 0
    property int score_support: 0
    property int score_recon: 0

    width: parent.width
    height: 200

    Grid {
        id: kitProgressLayout

        columns: 4
        rows: 1
        spacing: 10

        // calculate progress bar width
        function getBlockWidth() {
            return (parent.width - ((spacing * (columns - 1)))) / columns;
        }

        Component {
            id: kitProgressBar

            Item {
                id: kitProgressRoot

                property int score: 0
                property string kit: ""

                function getProgressBarHeight() {
                    var score = kitOverviewRoot['score_' + kit];
                    if (score > 0) {
                        var threshold = BFConstants.serviceStarThresholds[kit];

                        var stars = Math.floor(score/threshold);
                        var progressScore = score - (stars * threshold);

                        var progressPercent = (progressScore * 100) / threshold;

                        var maxHeight = kitPprogressBarWrapper.height - kitProgressBarValue.anchors.margins;
                        var progressBarHeight = Math.ceil((progressPercent * maxHeight) / 100);
                        progressBarHeight = Math.min(progressBarHeight, maxHeight);

                        return progressBarHeight;

                    } else {
                        return 0;
                    }
                }

                function getStarCount() {
                    var score = kitOverviewRoot['score_' + kit];
                    if (score > 0) {

                        var threshold = BFConstants.serviceStarThresholds[kit];
                        return Math.floor(score/threshold);

                    } else {
                        return 0;
                    }
                }

                function getAnimationDuration() {
                    var score = kitOverviewRoot['score_' + kit];
                    if (score > 0) {
                        var baseDuration = 700;
                        var threshold = BFConstants.serviceStarThresholds[kit];

                        var stars = Math.floor(score/threshold);
                        var progressScore = score - (stars * threshold);

                        var progressPercent = (progressScore * 100) / threshold;

                        return Math.round((baseDuration * progressPercent) / 100);

                    } else {
                        return 0;
                    }
                }

                width: kitProgressLayout.getBlockWidth()
                height: kitOverviewRoot.height

                Image {
                    id: kitLogo
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 10
                    anchors.horizontalCenter: parent.horizontalCenter

                    source: parent.kit ? "img/kit/" + parent.kit + ".png" : ""
                    sourceSize.width: 30
                    sourceSize.height: 30

                    width: 30
                    height: 30
                }

                Rectangle {
                    id: kitPprogressBarWrapper
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.bottom: kitLogo.top
                    anchors.bottomMargin: 10

                    color: "gray"
                    opacity: 0.1
                    radius: 9
                }

                Rectangle {
                    id: kitProgressBarValue
                    color: "#E1E9D3"
                    opacity: 0.3
                    radius: 9

                    anchors.margins: 4
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.bottom: kitPprogressBarWrapper.bottom

                    height: kitProgressRoot.getProgressBarHeight()

                    Behavior on height {
                        NumberAnimation { duration: kitProgressRoot.getAnimationDuration(); easing.type: Easing.OutCubic }
                    }

                    onVisibleChanged: {
                        if (parent.visible) {
                            height = parent.getProgressBarHeight()
                        } else {
                            height = 0
                        }
                    }
                }

                Image {
                    id: serviceStar
                    anchors.top: parent.top
                    anchors.topMargin: 12
                    anchors.horizontalCenter: parent.horizontalCenter

                    visible: kitProgressRoot.getStarCount() > 0

                    source: "img/servicestar_medium.png"
                    sourceSize.width: 80
                    sourceSize.height: 80

                    width: 80
                    height: 80

                    fillMode: Image.PreserveAspectFit
                    smooth: true

                    Behavior on width {
                        NumberAnimation { duration: 700; easing.type: Easing.OutBack }
                    }

                    onVisibleChanged: {
                        if (parent.visible) {
                            width = 80
                        } else {
                            width = 0
                        }
                    }
                }

                Rectangle {
                    color: "black"
                    opacity: 0.8
                    radius: 15

                    visible: kitProgressRoot.getStarCount() > 0

                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: serviceStar.bottom
                    anchors.topMargin: 5

                    width: 40
                    height: 35

                    Text {
                        text: kitProgressRoot.getStarCount()
                        color: "white"
                        font.bold: true
                        font.pixelSize: 23

                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                    }
                }
            }
        }

        Loader {
            sourceComponent: kitProgressBar

            Component.onCompleted: {
                item.kit = "assault"
                item.score = kitOverviewRoot.score_assault
            }
        }
        Loader {
            sourceComponent: kitProgressBar

            Component.onCompleted: {
                item.kit = "engineer"
                item.score = kitOverviewRoot.score_engineer
            }
        }
        Loader {
            sourceComponent: kitProgressBar

            Component.onCompleted: {
                item.kit = "support"
                item.score = kitOverviewRoot.score_support
            }
        }
        Loader {
            sourceComponent: kitProgressBar

            Component.onCompleted: {
                item.kit = "recon"
                item.score = kitOverviewRoot.score_recon
            }
        }
    }
}
