import QtQuick 1.1
import com.nokia.meego 1.0
import "UIConstants.js" as UIConstants
import "Utils.js" as Utils

Item {
    id: weaponListRoot
    width: parent.width
    height: parent.height - UIConstants.ToolbarHeight + 60

    function scrollTop() {
        vehicleListView.positionViewAtBeginning();
    }

    Component {
        id: vehicleDelegate

        Item {
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.rightMargin: UIConstants.DefaultMargin
            anchors.leftMargin: UIConstants.DefaultMargin
            height: 120

            Rectangle {
                id: rowBackground
                anchors.fill: parent
                radius: 9
                color: "gray"
                opacity: 0.1
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("VehiclePage.qml"), {vehicleData: model.data})
                }
                onPressed: { rowBackground.opacity = 0.3 }
                onReleased: { rowBackground.opacity = 0.1 }

                onContainsMouseChanged: {
                    if (!containsMouse) {
                        rowBackground.opacity = 0.1
                    }
                }
            }

            Item {
                id: vehicleImageWrapper
                width: 210
                height: parent.height
                anchors.left: parent.left
                anchors.top: parent.top

                Image {
                    anchors.top: parent.top
                    anchors.topMargin: 4
                    anchors.horizontalCenter: parent.horizontalCenter

                    source: "img/small/" + model.data.img
                    // original image size: 512x308
                    sourceSize.width: 155
                    sourceSize.height: 93

                    width: 155
                    height: 93
                }

                Item {
                    id: nameWrapper
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 7
                    width: parent.width

                    height: 20

                    Text {
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter

                        font.pixelSize: 17
                        color: "white"
                        text: model.data.name
                    }
                }
            }

            Item {
                id: timeWrapper
                width: 135
                height: parent.height
                anchors.left: vehicleImageWrapper.right
                anchors.top: parent.top

                Text {
                    id: timeValue
                    width: parent.width
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: parent.top
                    anchors.topMargin: 3
                    anchors.bottom: timeTitle.top
                    anchors.bottomMargin: 3

                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    clip: true

                    font.family: "Nokia Pure"
                    font.pixelSize: 24
                    font.letterSpacing: -3
                    color: "white"

                    wrapMode: Text.WordWrap

                    text: Utils.humanTime(model.data.time)

                    Component.onCompleted: {
                        timeValue.lineHeight = 0.8
                    }
                }

                Text {
                    id: timeTitle
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 10

                    font.capitalization: Font.AllUppercase
                    font.pixelSize: 17
                    color: "grey"

                    text: "Time used"
                }
            }

            Item {
                id: killsWrapper
                width: 120
                height: parent.height
                anchors.left: timeWrapper.right
                anchors.top: parent.top

                Text {
                    id: killsValue
                    width: parent.width
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: parent.top
                    anchors.topMargin: 3
                    anchors.bottom: killsTitle.top
                    anchors.bottomMargin: 3

                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    clip: true

                    font.family: "Nokia Pure"
                    font.pixelSize: 24
                    font.letterSpacing: -3
                    color: "white"

                    wrapMode: Text.WordWrap

                    text: model.data.kills

                    Component.onCompleted: {
                        killsValue.lineHeight = 0.8
                    }
                }

                Text {
                    id: killsTitle
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 10

                    font.capitalization: Font.AllUppercase
                    font.pixelSize: 17
                    color: "grey"

                    text: "Kills"
                }
            }
        }
    }

    ListView {
        id: vehicleListView
        anchors.fill: parent
        model: VehicleListModel {}
        delegate: vehicleDelegate
        spacing: 8
        snapMode: ListView.SnapToItem
        cacheBuffer: 1800 // cache 15 items both sides for smooth scrolling
        header: Fieldset {
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.rightMargin: UIConstants.DefaultMargin
            anchors.leftMargin: UIConstants.DefaultMargin
            title: "Vehicles ordered by kills"
        }
    }

    ScrollDecorator { flickableItem: vehicleListView }
}
