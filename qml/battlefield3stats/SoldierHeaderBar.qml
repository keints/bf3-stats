import QtQuick 1.0
import com.nokia.meego 1.0

Rectangle {
    id: headerBarRoot

    property string title: rootElement.currentSoldier ? rootElement.currentSoldier.name : ""
    property string platform: rootElement.currentSoldier ? rootElement.currentSoldier.platform : ""
    property color titleColor: "white"

    width: parent.width
    height: 70
    color: "orange"

    Text {
        id: title
        text: parent.title
        color: parent.titleColor
        font.pixelSize: 33
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: UiConstants.DefaultMargin
    }

    Rectangle {
        id: platformWrapper

        radius: 5
        width: 80
        height: 30

        border.width: 2
        border.color: "white"

        gradient: Gradient {
             GradientStop { position: 0.0; color: "#ff7800" }
             GradientStop { position: 1.0; color: "orange" }
         }

        anchors.verticalCenter: parent.verticalCenter
        anchors.left: title.right
        anchors.leftMargin: 25

        Image {
            id: platformLogo
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter

            source: "img/platform/icon_" + (headerBarRoot.platform ? headerBarRoot.platform : "pc") + ".png"
        }
    }

    Rectangle {
        id: buttonCover
        color: "orange"
        height: parent.height
        anchors.right: parent.right
        width: 75

        ToolIcon {
            id: filterImage
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            platformIconId: "icon-m-textinput-combobox-arrow"
        }
    }

    Image {
        id: textEndGradient
        source: "img/header-gradient.png"
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: buttonCover.left
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {

            var listModelComponent = Qt.createComponent("HeaderListModel.qml");
            var listModelObj = listModelComponent.createObject(pageStack.currentPage, {soldiers: rootElement.soldiers});

            var soldierSelectDialogComponent = Qt.createComponent("SoldierSelectDialog.qml");
            var soldierSelectDialogObj = soldierSelectDialogComponent.createObject(pageStack.currentPage, {model: listModelObj});
            soldierSelectDialogObj.open();
        }
        onPressed: { clickVisualState.visible = true }
        onReleased: { clickVisualState.visible = false }
    }

    Rectangle {
        id: clickVisualState
        anchors.fill: parent
        color: "black"
        opacity: 0.3
        visible: false
    }
}
