import QtQuick 1.1
import com.nokia.meego 1.0
import "UIConstants.js" as UIConstants

Item {
    width: parent.width
    height: parent.height

    function scrollTop() {
        pageContent.contentY = 0;
    }

    property variant soldierData: rootElement.currentSoldier.data

    Flickable {
        id: pageContent

        width: parent.width
        height: parent.height

        contentWidth: parent.width
        contentHeight: parent.height + 1120

        Item {
            id: flickableMargin
            anchors.fill: parent
            anchors.rightMargin: UIConstants.DefaultMargin
            anchors.leftMargin: UIConstants.DefaultMargin

            Fieldset {
                id: rankFieldset
                title: "Current rank"
            }

            Rank {
                id: currentRank
                nr: soldierData.stats.rank.nr
                name: soldierData.stats.rank.name
                image: soldierData.stats.rank.img_medium
                score: soldierData.stats.rank.score

                score_next: soldierData.stats.nextranks[0].score
                left_next: soldierData.stats.nextranks[0].left

                anchors.top: rankFieldset.bottom

            }

            Fieldset {
                id: nextRankFieldset
                title: "Next rank"
                anchors.top: currentRank.bottom
            }

            TinyRank {
                id: nextRank
                nr: soldierData.stats.nextranks[0].nr
                name: soldierData.stats.nextranks[0].name
                image: soldierData.stats.nextranks[0].img_medium
                score: soldierData.stats.nextranks[0].score
                score_left: soldierData.stats.nextranks[0].left

                anchors.top: nextRankFieldset.bottom
            }

            Fieldset {
                id: generalStatsFieldset
                title: "General statistics"
                anchors.top: nextRank.bottom
            }

            GeneralStats {
                id: generalStats
                time: soldierData.stats.global.time
                score: soldierData.stats.nextranks[0].score - soldierData.stats.nextranks[0].left
                kills: soldierData.stats.global.kills
                deaths: soldierData.stats.global.deaths
                wins: soldierData.stats.global.wins
                losses: soldierData.stats.global.losses
                shots: soldierData.stats.global.shots
                hits: soldierData.stats.global.hits

                anchors.top: generalStatsFieldset.bottom
            }

            Fieldset {
                id: kitOverviewFieldset
                title: "Kit overview"
                anchors.top: generalStats.bottom
            }

            KitOverview {
                id: kitOverview
                anchors.top: kitOverviewFieldset.bottom

                score_assault: soldierData.stats.kits.assault.score
                score_engineer: soldierData.stats.kits.engineer.score
                score_support: soldierData.stats.kits.support.score
                score_recon: soldierData.stats.kits.recon.score
            }

            Fieldset {
                id: scoreDetailsFieldset
                title: "Score details"
                anchors.top: kitOverview.bottom
            }

            ScoreDetails {
                id: scoreDetails
                anchors.top: scoreDetailsFieldset.bottom

                score_kits: (soldierData.stats.scores.assault +
                             soldierData.stats.scores.engineer +
                             soldierData.stats.scores.support +
                             soldierData.stats.scores.recon)
                score_vehicles: soldierData.stats.scores.vehicleall
                score_award: soldierData.stats.scores.award
                score_unlocks: soldierData.stats.scores.unlock
            }

            Fieldset {
                id: moreStatsFieldset
                title: "More statistics"
                anchors.top: scoreDetails.bottom
            }

            MoreStats {
                id: moreStats
                anchors.top: moreStatsFieldset.bottom

                kills: soldierData.stats.global.kills
                deaths: soldierData.stats.global.deaths
                kill_assists: soldierData.stats.global.killassists
                vehicles_destroyed: soldierData.stats.global.vehicledestroyed
                vehicles_destroyed_assists: soldierData.stats.global.vehicledestroyassist
                headshots: soldierData.stats.global.headshots
                longest_headshot: soldierData.stats.global.longesths
                kill_streak_bonus: soldierData.stats.global.killstreakbonus
                squad_score_bonus: soldierData.stats.scores.squad
                repairs: soldierData.stats.global.repairs
                revives: soldierData.stats.global.revives
                heals: soldierData.stats.global.heals
                resupplies: soldierData.stats.global.resupplies

            }
        }
    }

    ScrollDecorator { flickableItem: pageContent }

}
