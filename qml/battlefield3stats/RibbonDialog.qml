import QtQuick 1.1
import com.nokia.meego 1.0

Dialog {
    id: ribbonDialog
    width: parent.width - 40
    anchors.verticalCenter: parent.verticalCenter

    property variant ribbon: null

    title: Item {
        id: ribbonDialogTitle

        width: parent.width
        height: 55
        MarqueeText {
            id: ribbonDialogTitleText

            width: parent.width
            font.capitalization: Font.AllUppercase
            font.family: "Nokia Pure"
            font.pixelSize: 30
            font.letterSpacing: -1
            color: "white"

            textAlign: "left"
            interval: 100

            text: ribbonDialog.ribbon ? ribbonDialog.ribbon.name : ""
        }
        Rectangle {
            anchors.top: ribbonDialogTitleText.bottom
            anchors.topMargin: 2
            width: parent.width
            height: 1
            color: "gray"
        }
    }
    content: Item {
        height: ribbonDialogTitle.height + ribbonDialogImage.height + ribbonDialogDesc.height + countRow.height + timeRow.height

        Image {
            id: ribbonDialogImage
            source: ribbonDialog.ribbon ? "img/" + ribbonDialog.ribbon.img_large : ""
            sourceSize.width: 442
            sourceSize.height: 164

            width: 442
            height: 164
        }

        Text {
            id: ribbonDialogDesc
            anchors.top: ribbonDialogImage.bottom
            anchors.topMargin: 10
            width: ribbonDialog.width

            font.family: "Nokia Pure"
            font.pixelSize: 20
            font.letterSpacing: -1
            color: "gray"

            horizontalAlignment: Text.AlignJustify
            wrapMode: Text.WordWrap

            text: ribbonDialog.ribbon ? ribbonDialog.ribbon.desc : ""
        }



        DataRow {
            id: countRow

            width: ribbonDialog.width
            anchors.top: ribbonDialogDesc.bottom
            anchors.topMargin: 10

            title: "Ribbon count"
            value: ribbonDialog.ribbon ? ribbonDialog.ribbon.count : 0
        }

        DataRow {
            id: timeRow

            width: ribbonDialog.width
            anchors.top: countRow.bottom
            anchors.topMargin: 10

            function getAcquireDate() {
                var dateString = "Never"
                if (ribbonDialog.ribbon && ribbonDialog.ribbon.date > 0) {
                    var acquiredDate = new Date(ribbonDialog.ribbon.date * 1000);
                    var monthChunk = acquiredDate.getMonth() + 1;
                    monthChunk = monthChunk < 10 ? '0' + monthChunk : monthChunk;
                    var dateChunk = acquiredDate.getDate() < 10 ? '0' + acquiredDate.getDate() : acquiredDate.getDate();
                    dateString = acquiredDate.getFullYear() + '-' + monthChunk + '-' + dateChunk;
                }
                return dateString;
            }

            title: "First time acquired"
            value: getAcquireDate()
        }
    }

    buttons: ButtonRow {
        style: ButtonStyle { }
        anchors.horizontalCenter: parent.horizontalCenter
        Button {
            text: "Close";
            onClicked: ribbonDialog.close();
        }
    }

    onStatusChanged: {
        if (status == DialogStatus.Closing) {
            ribbonDialog.destroy();
        }
    }
}
