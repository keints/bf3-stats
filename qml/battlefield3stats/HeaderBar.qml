import QtQuick 1.0
import com.nokia.meego 1.0

Rectangle {
    id: headerBarRoot

    property string title: "Enter StatusBar title"
    property color titleColor: "white"
    property alias backgroundColor: headerBarRoot.color

    width: parent.width
    height: 70
    color: "orange"

    Text {
        id: title
        text: parent.title
        color: parent.titleColor
        font.pixelSize: 33
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: UiConstants.DefaultMargin
    }
}
