import QtQuick 1.0

Item {
    property string title
    property color color: "#4b4b4b"

    width: parent.width
    height: 25

    Text {
        id: separatorText
        text: parent.title
        color: parent.color
        font.pixelSize: 15
        font.weight: Font.Bold

        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
    }

    Rectangle {
        id: line
        width: parent.width
        height: 1
        color: parent.color

        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.right: separatorText.left
        anchors.rightMargin: 15
    }
}
