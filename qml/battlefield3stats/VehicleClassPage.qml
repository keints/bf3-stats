import QtQuick 1.1
import com.nokia.meego 1.0
import "UIConstants.js" as UIConstants
import "Utils.js" as Utils

MyPage {
    id: vehicleClassPage
    tools: singlePageTools

    property variant vehicleClassData: null

    SoldierHeaderBar {
        id: header
        z: 100
    }

    Flickable {
        id: pageContent
        width: parent.width
        height: parent.height - UIConstants.ToolbarHeight

        anchors.top: header.bottom

        contentWidth: parent.width
        contentHeight: 0

        Item {
            id: flickableMargin

            onHeightChanged: {
                pageContent.contentHeight = height
            }

            anchors.top: parent.top
            anchors.left: parent.left
            anchors.leftMargin: UIConstants.DefaultMargin
            anchors.right: parent.right
            anchors.rightMargin: UIConstants.DefaultMargin

            height: nameFieldset.height + vehicleClassName.height + statisticsFieldset.height + unlocksFieldset.height + 60

            Fieldset {
                id: nameFieldset
                title: "Vehicle Class"
            }

            Item {
                id: nameWrapper
                anchors.top: nameFieldset.bottom
                width: parent.width
                height: 40

                Image {
                    id: vehicleClassImage
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left

                    source: "img/" + vehicleClassPage.vehicleClassData.img
                    sourceSize.width: 50
                    sourceSize.height: 50

                    height: parent.height - 5

                    fillMode: Image.PreserveAspectFit
                    smooth: true
                }

                Text {
                    id: vehicleClassName
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: vehicleClassImage.right
                    anchors.leftMargin: 15

                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                    clip: true

                    font.family: "Nokia Pure"
                    font.pixelSize: 30
                    font.letterSpacing: -1
                    color: "white"

                    wrapMode: Text.WordWrap

                    text: vehicleClassPage.vehicleClassData.name
                }
            }

            Fieldset {
                id: statisticsFieldset
                title: "Statistics"
                anchors.top: nameWrapper.bottom
            }

            Column {
                id: vehicleClassStatistics
                anchors.top: statisticsFieldset.bottom
                width: parent.width

                spacing: 8

                // calculate infoblock height
                function getBlockHeight() {
                    return (height - ((spacing * (rows - 1)))) / rows;
                }

                function getStatistics() {
                    return [
                        {title: "Kills", value: Utils.beautifyNumber(vehicleClassPage.vehicleClassData.kills), font_size: 28 },
                        {title: "Score", value: Utils.beautifyNumber(vehicleClassPage.vehicleClassData.score) + " XP", font_size: 27 },
                        {title: "Time Used", value: Utils.humanTime(vehicleClassPage.vehicleClassData.time), font_size: 28 }
                    ];
                }

                Component {
                    id: statisticsBox

                    DataRow {
                        title: modelData.title
                        value: modelData.value
                    }
                }

                Repeater {
                    id: statisticsBoxRepeater
                    model: vehicleClassStatistics.getStatistics()
                    delegate: statisticsBox
                }

                Component.onCompleted: {
                    flickableMargin.height = flickableMargin.height + height
                }
            }

            Fieldset {
                id: serviceStarFieldset
                title: "Service Stars"
                anchors.top: vehicleClassStatistics.bottom

                visible: vehicleClassPage.vehicleClassData.star.count > 0

                Component.onCompleted: {
                    if (vehicleClassPage.vehicleClassData.star.count > 0) {
                        flickableMargin.height = flickableMargin.height + height
                    }
                }
            }

            Item {
                id: serviceStarWrapper

                width: parent.width
                height: 120
                anchors.top: serviceStarFieldset.bottom

                visible: vehicleClassPage.vehicleClassData.star.count > 0

                Rectangle {
                    anchors.left: parent.left
                    width: (parent.width / 2) - 5
                    height: parent.height
                    color: "gray"
                    opacity: 0.1
                    radius: 9
                }

                Rectangle {
                    id: rightBackground
                    anchors.right: parent.right
                    width: (parent.width / 2) - 5
                    height: parent.height
                    color: "gray"
                    opacity: 0.1
                    radius: 9
                }

                Image {
                    id: serviceStar
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 15

                    opacity: vehicleClassPage.vehicleClassData.star.count > 0 ? 1 : 0.2

                    source: "img/servicestar.png"
                    sourceSize.width: 256
                    sourceSize.height: 256

                    width: 100
                    height: 100

                    fillMode: Image.PreserveAspectFit
                    smooth: true
                }

                Rectangle {
                    color: "black"
                    opacity: 0.8
                    radius: 19

                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: serviceStar.right
                    anchors.leftMargin: 15

                    width: 60
                    height: 50

                    Text {
                        text: vehicleClassPage.vehicleClassData.star.count
                        color: "white"
                        font.bold: true
                        font.pixelSize: 30

                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                    }
                }

                Item {
                    width: rightBackground.width
                    height: rightBackground.height
                    anchors.right: parent.right

                    Text {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.top: parent.top
                        anchors.topMargin: 21

                        font.family: "Nokia Pure"
                        font.pixelSize: 35
                        font.letterSpacing: -3
                        color: "white"

                        wrapMode: Text.WordWrap

                        text: Utils.beautifyNumber(vehicleClassPage.vehicleClassData.star.needed - vehicleClassPage.vehicleClassData.star.curr)
                    }

                    Text {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 18

                        font.capitalization: Font.AllUppercase
                        font.pixelSize: 21
                        color: "grey"

                        text: "XP to next"
                    }
                }

                Component.onCompleted: {
                    if (vehicleClassPage.vehicleClassData.star.count > 0) {
                        flickableMargin.height = flickableMargin.height + height
                    }
                }
            }

            Fieldset {
                id: unlocksFieldset
                title: "Vehicle Class Unlocks"
                anchors.top: vehicleClassPage.vehicleClassData.star.count > 0 ? serviceStarWrapper.bottom : vehicleClassStatistics.bottom;
            }

            Grid {
                id: vehicleClassUnlocks
                width: parent.width
                anchors.top: unlocksFieldset.bottom

                columns: 3
                spacing: 10

                function getUnlocks() {
                    return vehicleClassPage.vehicleClassData.unlocks;
                }

                Component {
                    id: unlockCell

                    Item {
                        id: unlockBox
                        width: 149
                        height: 95

                        function getProgressWidth() {
                            if (!modelData.needed) {
                                return 0;

                            } else if (modelData.curr == modelData.needed) {
                                return width - 2 * unlockProgress.anchors.margins;

                            } else {

                                var percent = (modelData.curr * 100) / modelData.needed;
                                var maxWidth = width - 2 * unlockProgress.anchors.margins;
                                var progressWidth = (maxWidth * percent) / 100;

                                return Math.min(maxWidth, progressWidth);
                            }
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                var unlockDialogComponent = Qt.createComponent("UnlockDialog.qml");
                                var unlockDialogObj = unlockDialogComponent.createObject(vehicleClassPage, {unlock: modelData});
                                unlockDialogObj.open();
                            }
                            onPressed: { cellBackground.opacity = 0.4 }
                            onReleased: { cellBackground.opacity = 0.1 }

                            onContainsMouseChanged: {
                                if (!containsMouse) {
                                    cellBackground.opacity = 0.1
                                }
                            }
                        }

                        Rectangle {
                            id: cellBackground
                            anchors.fill: parent
                            color: "gray"
                            opacity: 0.1
                            radius: 9
                        }

                        Rectangle {
                            id: unlockProgress
                            color: "#E1E9D3"
                            opacity: (modelData.needed && modelData.curr == modelData.needed) ? 0.5 : 0.3
                            radius: 9

                            anchors.margins: 4
                            anchors.left: parent.left
                            anchors.top: parent.top
                            anchors.bottom: parent.bottom

                            width: unlockBox.getProgressWidth()
                        }

                        Image {
                            id: unlockImage
                            anchors.top: parent.top
                            anchors.topMargin: 10
                            anchors.horizontalCenter: parent.horizontalCenter

                            source: "img/" + modelData.img
                            sourceSize.width: 87
                            sourceSize.height: 52

                            width: 87
                            height: 52
                        }

                        MarqueeText {
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: 7
                            width: parent.width - 10

                            font.capitalization: Font.AllUppercase
                            font.pixelSize: 17
                            color: "white"

                            text: modelData.name
                        }
                    }
                }

                Repeater {
                    id: unlocksRepeater
                    model: vehicleClassUnlocks.getUnlocks()
                    delegate: unlockCell

                    onItemAdded: {
                        if (!(index % vehicleClassUnlocks.columns)) {
                            var addedHeight = itemAt(index).height;
                            if (index > 0) {
                                addedHeight += vehicleClassUnlocks.spacing;
                            }
                            flickableMargin.height = flickableMargin.height + addedHeight
                        }
                    }
                }
            }
        }
    }

    ScrollDecorator { flickableItem: pageContent }
}
