import QtQuick 1.1
import com.nokia.meego 1.0
import "UIConstants.js" as UIConstants

MyTabPage {
    name: "overview"

    function scrollToTop() {
        pageLoader.item.scrollTop();
    }

    SoldierHeaderBar {
        id: header
        z: 100
    }

    MyBusyIndicator {
        id: busyIndicator
        visible: true
    }

    Loader {
        id: pageLoader

        width: parent.width
        height: parent.height - UIConstants.ToolbarHeight
        anchors.top: header.bottom

        onStatusChanged: {
            if (status == Loader.Ready) {
                busyIndicator.visible = false;
            }
        }
    }

    Timer {
        id: loadingTimer
        interval: 50
        running: false
        repeat: false
        onTriggered: {
            if (pageLoader.source == "") {
                pageLoader.source = "OverviewPageContent.qml";
                running = false;
            }
        }
    }

    Component.onCompleted: {
        loadingTimer.running = true;
    }
}
