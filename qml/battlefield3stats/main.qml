import QtQuick 1.1
import com.nokia.meego 1.0
import "Globals.js" as Globals
import "Utils.js" as Utils
import "Storage.js" as Storage

PageStackWindow {
    id: rootElement
    initialPage: splashPage

    property int statusbarMargin: 35

    property variant soldiers: null
    property variant currentSoldier: null

    function changeCurrentSoldier(soldierId) {
        currentSoldier = Storage.setCurrentSoldier(soldierId);
        soldiers = Storage.getSoldiersList();
    }

    function refreshSoldiersData(forceDefault) {
        var fDefault = forceDefault ? true : false;
        currentSoldier = Storage.getCurrentSoldier(fDefault);
        soldiers = Storage.getSoldiersList();
    }

    platformStyle: PageStackWindowStyle {
        background: pageStack.currentPage && pageStack.currentPage.name == "soldier" ? "img/bg/soldier-select.jpg" : "img/bg/default.jpg"
        backgroundFillMode: Image.PreserveAspectCrop
        cornersVisible: true
    }
    showStatusBar: false

    Component.onCompleted: {
        theme.inverted = true
        theme.colorScheme = "orange"
    }

    ToolBarLayout {
        visible: false
        id: commonTools

        function checkFirstTab() {
            overviewTabButton.checked = true;
        }

        ButtonRow {
            MyTabButton {
                id: overviewTabButton
                tab: null
                pageName: "overview"
                title: "Overview"
            }
            MyTabButton {
                tab: null
                pageName: "weapons"
                title: "Weapons"
            }
            MyTabButton {
                tab: null
                pageName: "vehicles"
                title: "Vehicles"
            }
            MyTabButton {
                tab: null
                pageName: "awards"
                title: "Awards"
            }
        }

        ToolIcon {
            iconId: "icon-m-toolbar-view-menu-white"
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            onClicked: (myMenu.status == DialogStatus.Closed) ? myMenu.open() : myMenu.close()
        }
    }

    ToolBarLayout {
        visible: false
        id: singlePageTools

        ToolIcon {
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            iconId: theme.inverted ? "icon-m-toolbar-back-white" : "icon-m-toolbar-back";
            onClicked: { pageStack.pop(); }
        }
    }

    Menu {
        id: myMenu
        MenuLayout {
            MenuItem {
                text: "Quit"
                onClicked: Qt.quit()
            }
            MenuItem {
                text: "Refresh data"
                onClicked: {
                }
            }
            MenuItem {
                text: "Manage soldiers"
                onClicked: pageStack.push(Qt.resolvedUrl("SoldierListPage.qml"))
            }
            MenuItem {
                text: "Soldier search"
                onClicked: {
                    var page = rootElement.getPage("soldier");
                    pageStack.push(page, {showToolbar: true});
                }
            }
            MenuItem {
                text: "About"
                onClicked: {
                    var aboutDialogComponent = Qt.createComponent("AboutDialog.qml");
                    var aboutDialogObj = aboutDialogComponent.createObject(pageStack.currentPage);
                    aboutDialogObj.open();
                }
            }
        }
    }

    SplashPage {
        id: splashPage
    }

    function getPage(pageName) {
        if (!Globals.createdPages[pageName]) {
            var pageComponent = Qt.createComponent(Utils.ucFirst(pageName) + "Page.qml");
            var page = pageComponent.createObject(pageStack);
            Globals.createdPages[pageName] = page;
        }
        return Globals.createdPages[pageName];
    }
}
