import QtQuick 1.1
import com.nokia.meego 1.0
import "UIConstants.js" as UIConstants
import "Utils.js" as Utils

MyPage {
    id: vehiclePage
    tools: singlePageTools

    property variant vehicleData: null

    SoldierHeaderBar {
        id: header
        z: 100
    }

    Flickable {
        id: pageContent
        width: parent.width
        height: parent.height - UIConstants.ToolbarHeight

        anchors.top: header.bottom

        contentWidth: parent.width
        contentHeight: 0

        Item {
            id: flickableMargin

            onHeightChanged: {
                pageContent.contentHeight = height
            }

            anchors.top: parent.top
            anchors.left: parent.left
            anchors.leftMargin: UIConstants.DefaultMargin
            anchors.right: parent.right
            anchors.rightMargin: UIConstants.DefaultMargin

            height: vehicleImageFieldset.height + vehicleImageBackground.height + nameDescriptionFieldset.height + vehicleName.height + statisticsFieldset.height + 60

            Fieldset {
                id: vehicleImageFieldset
                title: "Image"
            }

            Rectangle {
                id: vehicleImageBackground
                color: "gray"
                opacity: 0.1
                width: parent.width
                height: 270
                radius: 9
                anchors.top: vehicleImageFieldset.bottom
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Image {
                id: vehicleImage
                anchors.verticalCenter: vehicleImageBackground.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter

                source: "img/" + vehiclePage.vehicleData.img
                sourceSize.width: 426
                sourceSize.height: 256

                width: 426
                height: 256
            }

            Fieldset {
                id: nameDescriptionFieldset
                title: "Name & Description"
                anchors.top: vehicleImageBackground.bottom
            }

            Text {
                id: vehicleName
                anchors.top: nameDescriptionFieldset.bottom
                anchors.left: parent.left

                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                clip: true

                font.family: "Nokia Pure"
                font.pixelSize: 30
                font.letterSpacing: -1
                color: "white"

                wrapMode: Text.WordWrap

                text: vehiclePage.vehicleData.name
            }

            Text {
                id: vehicleDesc
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: vehicleName.bottom

                horizontalAlignment: Text.AlignJustify
                verticalAlignment: Text.AlignVCenter
                clip: true

                font.family: "Nokia Pure"
                font.pixelSize: 20
                font.letterSpacing: -1
                color: "gray"

                wrapMode: Text.WordWrap

                text: vehiclePage.vehicleData.desc

                Component.onCompleted: {
                    if (text != "none") {
                        flickableMargin.height = flickableMargin.height + height
                        visible = true;
                    } else {
                        visible = false;
                    }
                }
            }

            Fieldset {
                id: statisticsFieldset
                title: "Statistics"
                anchors.top: vehicleDesc.visible ? vehicleDesc.bottom : vehicleName.bottom
            }

            Grid {
                id: vehicleStatistics
                anchors.top: statisticsFieldset.bottom
                width: parent.width
                height: (rows * 100)

                columns: 3
                rows: 1
                spacing: 10

                // calculate infoblock width
                function getBlockWidth() {
                    return (width - ((spacing * (columns - 1)))) / columns;
                }

                // calculate infoblock height
                function getBlockHeight() {
                    return (height - ((spacing * (rows - 1)))) / rows;
                }

                function getStatistics() {
                    return [
                        {title: "Time Used", value: Utils.humanTime(vehiclePage.vehicleData.time), font_size: 28 },
                        {title: "Kills", value: Utils.beautifyNumber(vehiclePage.vehicleData.kills), font_size: 28 },
                        {title: "Destroys", value: Utils.beautifyNumber(vehiclePage.vehicleData.destroys), font_size: 28 }
                    ];
                }

                Component {
                    id: statisticsBox

                    Item {
                        width: vehicleStatistics.getBlockWidth()
                        height: vehicleStatistics.getBlockHeight()

                        Rectangle {
                            anchors.fill: parent
                            color: "gray"
                            opacity: 0.1
                            radius: 9
                        }

                        Text {
                            id: boxValue
                            width: parent.width
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.top: parent.top
                            anchors.topMargin: 3
                            anchors.bottom: boxTitle.top
                            anchors.bottomMargin: 3

                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            clip: true

                            font.family: "Nokia Pure"
                            font.pixelSize: modelData.font_size ? modelData.font_size : 35
                            font.letterSpacing: -3
                            color: "white"

                            wrapMode: Text.WordWrap

                            text: modelData.value

                            Component.onCompleted: {
                                boxValue.lineHeight = 0.8
                            }
                        }

                        Text {
                            id: boxTitle
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: 7

                            font.capitalization: Font.AllUppercase
                            font.pixelSize: 17
                            color: "grey"

                            text: modelData.title
                        }
                    }
                }

                Repeater {
                    id: statisticsBoxRepeater
                    model: vehicleStatistics.getStatistics()
                    delegate: statisticsBox
                }

                Component.onCompleted: {
                    flickableMargin.height = flickableMargin.height + height
                }
            }

            Fieldset {
                id: vehicleClassFieldset
                title: "Vehicle Class"
                anchors.top: vehicleStatistics.bottom
                visible: (!!vehiclePage.vehicleData.category)

                Component.onCompleted: {
                    if (!!vehiclePage.vehicleData.category) {
                        flickableMargin.height = flickableMargin.height + height
                    }
                }
            }

            Item {
                anchors.top: vehicleClassFieldset.bottom
                width: parent.width
                height: 100
                visible: (!!vehiclePage.vehicleData.category)

                property variant classData: getClassData()

                function getClassData() {
                    var vehicleClass = vehiclePage.vehicleData.category;
                    if (!!vehicleClass) {
                        var data = rootElement.currentSoldier.data;

                        if (data.stats.vehcats && data.stats.vehcats[vehicleClass]) {
                            return data.stats.vehcats[vehicleClass];
                        }
                    }
                    return null;
                }

                Rectangle {
                    id: classBackground
                    anchors.fill: parent
                    radius: 9
                    color: "gray"
                    opacity: 0.1
                }

                Rectangle {
                    id: classImageBackground
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 10
                    width: parent.height - 20
                    height: parent.height - 20

                    radius: 9
                    color: "gray"
                    opacity: 0.1
                }

                Image {
                    id: vehicleClassLogo
                    anchors.verticalCenter: classImageBackground.verticalCenter
                    anchors.horizontalCenter: classImageBackground.horizontalCenter

                    source: parent.classData ? "img/" + parent.classData.img : ""
                    sourceSize.width: 50
                    sourceSize.height: 50

                    width: parent.height + 10

                    fillMode: Image.PreserveAspectFit
                    smooth: true
                }

                Text {
                    id: className
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: classImageBackground.right
                    anchors.leftMargin: 20
                    height: parent.height - 20
                    width: 290

                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                    clip: true

                    font.family: "Nokia Pure"
                    font.pixelSize: 33
                    font.letterSpacing: -2
                    color: "white"

                    wrapMode: Text.WordWrap

                    text: parent.classData ? parent.classData.name : ""

                    Component.onCompleted: {
                        lineHeight = 0.8
                    }
                }

                Image {
                    id: vehicleClassArrow

                    property bool animating: false
                    property int originalX: parent.x + parent.width - 50

                    anchors.verticalCenter: classImageBackground.verticalCenter
                    x: parent.x + parent.width - 50

                    source: "img/arrow.png"
                    sourceSize.width: 33
                    sourceSize.height: 50

                    width: 33
                    height: 50

                    Behavior on x {
                        NumberAnimation { duration: 250; easing.type: Easing.InOutQuad }
                    }

                    onXChanged: {
                        if ((x == originalX) && animating) {
                            x = originalX + 10;
                        } else if (x == originalX + 10) {
                            x = originalX;
                        }
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("VehicleClassPage.qml"), {vehicleClassData: parent.classData})
                    }
                    onPressed: {
                        classBackground.opacity = 0.3;

                        if (vehicleClassArrow.x == vehicleClassArrow.originalX) {
                            vehicleClassArrow.x = vehicleClassArrow.originalX + 10;
                        }
                        vehicleClassArrow.animating = true;
                    }
                    onReleased: {
                        classBackground.opacity = 0.1;
                        vehicleClassArrow.animating = false;
                    }

                    onContainsMouseChanged: {
                        if (!containsMouse) {
                            classBackground.opacity = 0.1;
                            vehicleClassArrow.animating = false;
                        }
                    }
                }
            }
        }
    }

    ScrollDecorator { flickableItem: pageContent }
}
