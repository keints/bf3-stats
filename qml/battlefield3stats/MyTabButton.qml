import QtQuick 1.0
import com.nokia.meego 1.0

TabButton {
    tab: null
    property string pageName: ""
    property alias title: tabText.text
    property alias titleColor: tabText.color

    Image {
        id: tabImage
        source: parent.checked ? "img/tab-" + parent.pageName + "-active.png" : "img/tab-" + parent.pageName + ".png"
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 12
        anchors.bottomMargin: 13
        width: 25
        height: 25
    }
    Text {
        id: tabText
        text: "Overview"
        anchors.top: tabImage.bottom
        font.pixelSize: 16
        anchors.horizontalCenter: parent.horizontalCenter
        color: parent.checked ? "white" : "grey"
    }

    onClicked: {
        if (pageStack.currentPage.name != pageName) {
            var page = rootElement.getPage(pageName);
            pageStack.replace(page, {}, true);
        } else {
            if (pageStack.currentPage.scrollToTop) {
                pageStack.currentPage.scrollToTop();
            }
        }
    }
}
