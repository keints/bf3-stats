import QtQuick 1.1
import com.nokia.meego 1.0
import "UIConstants.js" as UIConstants

Item {
    anchors.left: parent.left
    anchors.leftMargin: UIConstants.DefaultMargin
    anchors.right: parent.right
    anchors.rightMargin: UIConstants.DefaultMargin
    anchors.top: parent.top
    anchors.bottom: parent.bottom
    anchors.bottomMargin: UIConstants.DefaultMargin

    function getRewardsData() {
        return rootElement.currentSoldier.data;
    }

    property variant rewardsData: getRewardsData()

    Fieldset {
        id: ribbonsFieldset
        title: ribbonsSummary.getMain() ? "Most frequent ribbons" : "Ribbons"
        anchors.top: parent.top
    }

    Item {
        id: ribbonsSummary

        anchors.top: ribbonsFieldset.bottom

        width: parent.width
        height: (parent.height - ribbonsFieldset.height - medalsFieldset.height) / 2

        function getRibbons() {
            var data = parent.rewardsData;

            // convert hash to array
            var ribbons = [];
            if (data && data.stats && data.stats.ribbons) {
                for (var key in data.stats.ribbons) {
                    ribbons.push(data.stats.ribbons[key]);
                }
            }

            return ribbons;
        }

        function orderByCount(ribbons) {
            // order by count desc
            for (var i = 0; i < ribbons.length; i++) {
                for (var j = i; j < ribbons.length; j++) {
                    if (ribbons[j].count > ribbons[i].count) {
                        var tmp = ribbons[i];
                        ribbons[i] = ribbons[j];
                        ribbons[j] = tmp;
                    }
                }
            }
            return ribbons;
        }

        function getSupplementary() {
            if (this.ribbons.length) {
                var orderedRibbons = orderByCount(this.ribbons);
                return orderedRibbons[1];
            }
            return null;
        }

        function getMain() {
            if (this.ribbons.length) {
                var orderedRibbons = orderByCount(this.ribbons);
                return orderedRibbons[0];
            }
            return null;
        }

        property variant ribbons: getRibbons()

        MarqueeText {
            id: mainRibbonName
            anchors.left: mainRibbonImage.left
            anchors.right: mainRibbonImage.right
            anchors.top: ribbonsSummary.top
            anchors.topMargin: 8
            textAlign: "center"

            font.pixelSize: 26
            color: "white"

            visible: parent.getMain() ? true : false

            text: parent.getMain() ? parent.getMain().name : ""
        }

        Image {
            id: mainRibbonImage
            anchors.top: mainRibbonName.bottom
            anchors.horizontalCenter: parent.horizontalCenter

            visible: parent.getMain() ? true : false

            source: parent.getMain() ? "img/" + parent.getMain().img_large : ""
            sourceSize.width: 442
            sourceSize.height: 164

            width: 442
            height: 164
        }

        MarqueeText {
            id: supplementaryRibbonName

            visible: parent.getMain() ? true : false

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: mainRibbonImage.bottom
            anchors.topMargin: 6

            width: supplementaryRibbonImage.width
            textAlign: "center"

            font.pixelSize: 19
            color: "white"

            text: parent.getSupplementary() ? parent.getSupplementary().name : ""
        }

        Image {
            id: supplementaryRibbonImage
            visible: parent.getMain() ? true : false

            anchors.top: supplementaryRibbonName.bottom
            anchors.topMargin: 2
            anchors.horizontalCenter: parent.horizontalCenter
            source: parent.getSupplementary() ? "img/small/" + parent.getSupplementary().img_large : ""
            sourceSize.width: 226
            sourceSize.height: 84
            width: 226
            height: 84
        }

        Rectangle {
            id: moreRibbonsHitArea
            anchors.fill: parent

            color: "gray"
            opacity: 0.1
            radius: 9
        }

        Image {
            id: arrowImg

            property bool animating: false
            property int originalX: null

            anchors.top: parent.top
            anchors.topMargin: ((parent.height - viewMoreRibbonsText.height - height) / 2) - 10
            x: parent.x + ((parent.width - width) / 2)

            visible: parent.getMain() ? false : true

            source: "img/arrow.png"
            sourceSize.width: 40
            sourceSize.height: 60

            width: 40
            height: 60

            Behavior on x {
                NumberAnimation { duration: 250; easing.type: Easing.InOutQuad }
            }

            onXChanged: {
                if ((x == originalX) && animating) {
                    x = originalX + 10;
                } else if (x == originalX + 10) {
                    x = originalX;
                }
            }

            Component.onCompleted: {
                originalX = x;
            }
        }

        Text {
            id: viewMoreRibbonsText
            width: parent.width - 100
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: arrowImg.bottom
            anchors.topMargin: 10

            font.family: "Nokia Pure"
            font.pixelSize: 28
            horizontalAlignment: Text.AlignHCenter
            color: "lightgray"

            wrapMode: Text.WordWrap
            visible: parent.getMain() ? false : true

            text: parent.getMain() ? qsTr("View all available ribbons") : qsTr("You have not unlocked any ribbons yet. Click here to view all available ribbons.")
        }

        MouseArea {
            anchors.fill: parent

            function showViewMoreHint() {
                if (mainRibbonName.visible) {
                    mainRibbonName.opacity = 0.2
                    mainRibbonImage.opacity = 0.2
                    supplementaryRibbonName.opacity = 0.2
                    supplementaryRibbonImage.opacity = 0.2
                }
                viewMoreRibbonsText.visible = true
                arrowImg.visible = true
            }

            function hideViewMoreHint() {
                if (mainRibbonName.visible) {
                    mainRibbonName.opacity = 1
                    mainRibbonImage.opacity = 1
                    supplementaryRibbonName.opacity = 1
                    supplementaryRibbonImage.opacity = 1
                }
                if (parent.getMain()) {
                    viewMoreRibbonsText.visible = false
                    arrowImg.visible = false
                }
            }

            onClicked: {
                var pageComponent = Qt.createComponent("RibbonsPage.qml");
                var page = pageComponent.createObject(pageStack, {ribbonsData: parent.ribbons});
                pageStack.push(page);
            }
            onPressed: {

                showViewMoreHint();

                moreRibbonsHitArea.opacity = 0.3
                if (arrowImg.x == arrowImg.originalX) {
                    arrowImg.x = arrowImg.originalX + 10;
                }
                arrowImg.animating = true;
            }
            onReleased: {
                hideViewMoreHint();

                moreRibbonsHitArea.opacity = 0.1
                arrowImg.animating = false;
            }

            onContainsMouseChanged: {
                if (!containsMouse) {
                    hideViewMoreHint();

                    moreRibbonsHitArea.opacity = 0.1
                    arrowImg.animating = false;
                }
            }
        }
    }




    Fieldset {
        id: medalsFieldset

        title: medalsSummary.getMedals().length > 0 ? "Most frequent medals" : "Medals"
        anchors.top: ribbonsSummary.bottom
    }

    Item {
        id: medalsSummary

        width: parent.width
        height: (parent.height - ribbonsFieldset.height - medalsFieldset.height) / 2
        anchors.top: medalsFieldset.bottom

        function getMedals() {
            var data = parent.rewardsData;

            // convert hash to array
            var medals = [];
            if (data && data.stats && data.stats.medals) {
                for (var key in data.stats.medals) {
                    medals.push(data.stats.medals[key]);
                }
            }
            return medals;
        }

        function orderByCount(medals) {
            // order by count desc
            for (var i = 0; i < medals.length; i++) {
                for (var j = i; j < medals.length; j++) {
                    if (medals[j].count > medals[i].count) {
                        var tmp = medals[i];
                        medals[i] = medals[j];
                        medals[j] = tmp;
                    }
                }
            }
            return medals;
        }

        Grid {
            id: medalsGrid
            width: parent.width
            anchors.top: parent.top
            anchors.topMargin: 40

            visible: (parent.getMedals().length > 0)

            function getItems() {
                var medals = parent.getMedals();
                var orderedMedals = parent.orderByCount(medals);
                return orderedMedals.slice(0,2);
            }

            columns: 2
            spacing: 15

            Component {
                id: supplementaryMedal

                Item {
                    width: 225
                    height: 240

                    MarqueeText {
                        id: supplementaryMedalName
                        anchors.left: parent.left
                        anchors.leftMargin: 2
                        width: parent.width
                        textAlign: "center"

                        font.pixelSize: 26
                        color: "white"

                        text: modelData.name
                    }

                    Image {
                        anchors.top: supplementaryMedalName.bottom
                        anchors.topMargin: 18
                        anchors.horizontalCenter: parent.horizontalCenter
                        source: "img/small/" + modelData.img_large
                        sourceSize.width: 206
                        sourceSize.height: 206
                        width: 206
                        height: 206
                    }
                }

            }

            Repeater {
                id: medalsRepeater
                model: parent.getItems()
                delegate: supplementaryMedal
            }
        }

        Rectangle {
            id: moreMedalsHitArea
            anchors.fill: parent

            color: "gray"
            opacity: 0.1
            radius: 9
        }

        Image {
            id: arrowImgMedals

            property bool animating: false
            property int originalX: null

            anchors.top: parent.top
            anchors.topMargin: ((parent.height - viewMoreMedalsText.height - height) / 2) - 10
            x: parent.x + ((parent.width - width) / 2)

            visible: parent.getMedals().length ? false : true

            source: "img/arrow.png"
            sourceSize.width: 40
            sourceSize.height: 60

            width: 40
            height: 60

            smooth: true

            Behavior on x {
                NumberAnimation { duration: 250; easing.type: Easing.InOutQuad }
            }

            onXChanged: {
                if ((x == originalX) && animating) {
                    x = originalX + 10;
                } else if (x == originalX + 10) {
                    x = originalX;
                }
            }

            Component.onCompleted: {
                originalX = x;
            }
        }

        Text {
            id: viewMoreMedalsText
            width: parent.width - 100
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: arrowImgMedals.bottom
            anchors.topMargin: 10

            font.family: "Nokia Pure"
            font.pixelSize: 28
            horizontalAlignment: Text.AlignHCenter
            color: "lightgray"

            wrapMode: Text.WordWrap
            visible: parent.getMedals().length ? false : true

            text: parent.getMedals().length ? qsTr("View all available medals") : qsTr("You have not unlocked any medals yet. Click here to view all available medals.")
        }

        MouseArea {
            anchors.fill: parent

            function showViewMoreHint() {
                if (medalsGrid.visible) {
                    medalsGrid.opacity = 0.2
                }
                viewMoreMedalsText.visible = true
                arrowImgMedals.visible = true
            }

            function hideViewMoreHint() {
                if (medalsGrid.visible) {
                    medalsGrid.opacity = 1
                }
                if (parent.getMedals().length) {
                    viewMoreMedalsText.visible = false
                    arrowImgMedals.visible = false
                }
            }

            onClicked: {
                var pageComponent = Qt.createComponent("MedalsPage.qml");
                var page = pageComponent.createObject(pageStack, {medalsData: parent.getMedals()});
                pageStack.push(page);
            }
            onPressed: {
                showViewMoreHint();

                moreMedalsHitArea.opacity = 0.3
                if (arrowImgMedals.x == arrowImgMedals.originalX) {
                    arrowImgMedals.x = arrowImgMedals.originalX + 10;
                }
                arrowImgMedals.animating = true;
            }
            onReleased: {
                hideViewMoreHint();

                moreMedalsHitArea.opacity = 0.1
                arrowImgMedals.animating = false;
            }

            onContainsMouseChanged: {
                if (!containsMouse) {
                    hideViewMoreHint();

                    moreMedalsHitArea.opacity = 0.1
                    arrowImgMedals.animating = false;
                }
            }
        }
    }


}
