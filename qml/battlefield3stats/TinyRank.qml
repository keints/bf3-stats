import QtQuick 1.0
import "Utils.js" as Utils

Item {
    id: tinyRankRoot
    property int nr: 44
    property string name: "MAJOR"
    property string image: "ranksmedium/r44.png"
    property int score: 1600000
    property int score_left: 11010

    width: parent.width
    height: 90

    Image {
        id: rankLogo
        anchors.verticalCenter: parent.verticalCenter
        x: parent.x + ((128 - 90) / 2)

        source: "img/" + parent.image
        sourceSize.width: 90
        sourceSize.height: 90

        width: 90
        height: 90

        fillMode: Image.PreserveAspectFit
        smooth: true

        Behavior on width {
            NumberAnimation { duration: 500; easing.type: Easing.OutCubic }
        }

        onVisibleChanged: {
            if (tinyRankRoot.visible) {
                width = 90
            } else {
                width = 70
            }
        }
    }

    Item {
        anchors.left: rankLogo.right
        anchors.leftMargin: 27
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        height: 50

        Text {
            id: xpNumberText
            color: "orange"

            font.pixelSize: 24
            font.bold: true
            text: Utils.beautifyNumber(tinyRankRoot.score_left) + " XP"

            anchors.top: parent.top
            anchors.left: parent.left
        }

        Text {
            id: xpLeftText
            color: "white"

            font.pixelSize: 20
            text: "left to rank " + tinyRankRoot.nr

            anchors.bottom: xpNumberText.bottom
            anchors.left: xpNumberText.right
            anchors.leftMargin: 6
        }

        Text {
            id: nextRankText
            color: "gray"

            font.pixelSize: 17
            text: tinyRankRoot.name

            anchors.top: xpNumberText.bottom
            anchors.topMargin: 2
            anchors.left: parent.left
        }
    }
}
