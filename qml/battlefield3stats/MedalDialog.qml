import QtQuick 1.1
import com.nokia.meego 1.0
import "Utils.js" as Utils

Dialog {
    id: medalDialog
    width: parent.width - 40
    anchors.verticalCenter: parent.verticalCenter

    property variant medal: null

    title: Item {
        id: medalDialogTitle

        width: parent.width
        height: 55
        MarqueeText {
            id: medalDialogTitleText

            width: parent.width
            font.capitalization: Font.AllUppercase
            font.family: "Nokia Pure"
            font.pixelSize: 30
            font.letterSpacing: -1
            color: "white"

            textAlign: "left"
            interval: 100

            text: medalDialog.medal ? medalDialog.medal.name : ""
        }
        Rectangle {
            anchors.top: medalDialogTitleText.bottom
            anchors.topMargin: 2
            width: parent.width
            height: 1
            color: "gray"
        }
    }
    content: Item {
        height: medalDialogTitle.height + medalDialogImage.height + medalDialogDesc.height + medalCountRow.height + medalTimeRow.height + medalProgressRow.height

        Image {
            id: medalDialogImage
            source: medalDialog.medal ? "img/" + medalDialog.medal.img_large : ""
            sourceSize.width: 422
            sourceSize.height: 422

            width: 422
            height: 422
        }

        Text {
            id: medalDialogDesc
            anchors.top: medalDialogImage.bottom
            anchors.topMargin: 10
            width: medalDialog.width

            font.family: "Nokia Pure"
            font.pixelSize: 20
            font.letterSpacing: -1
            color: "gray"

            horizontalAlignment: Text.AlignJustify
            wrapMode: Text.WordWrap

            text: medalDialog.medal ? medalDialog.medal.desc : ""
        }



        DataRow {
            id: medalCountRow

            width: medalDialog.width
            anchors.top: medalDialogDesc.bottom
            anchors.topMargin: 10

            title: "Medal count"
            value: medalDialog.medal ? medalDialog.medal.count : 0
        }

        DataRow {
            id: medalTimeRow

            width: medalDialog.width
            anchors.top: medalCountRow.bottom
            anchors.topMargin: 10

            function getAcquireDate() {
                var dateString = "Never"
                if (medalDialog.medal && medalDialog.medal.date > 0) {
                    var acquiredDate = new Date(medalDialog.medal.date * 1000);
                    var monthChunk = acquiredDate.getMonth() + 1;
                    monthChunk = monthChunk < 10 ? '0' + monthChunk : monthChunk;
                    var dateChunk = acquiredDate.getDate() < 10 ? '0' + acquiredDate.getDate() : acquiredDate.getDate();
                    dateString = acquiredDate.getFullYear() + '-' + monthChunk + '-' + dateChunk;
                }
                return dateString;
            }

            title: "First time acquired"
            value: getAcquireDate()
        }

        DataRow {
            id: medalProgressRow

            width: medalDialog.width
            anchors.top: medalTimeRow.bottom
            anchors.topMargin: 10

            function getValue() {
                if (medalDialog.medal) {
                    var needed = medalDialog.medal.needed;
                    var curr = medalDialog.medal.curr;

                    if (medalDialog.medal.type == "once") {
                        needed = (needed / 60) / 60;
                        curr = Math.floor((curr / 60) / 60);
                    } else {
                        needed = Utils.beautifyNumber(needed);
                        curr =  Utils.beautifyNumber(curr);
                    }

                    return curr + " / " + needed;
                }
                return "";
            }

            title: "Progress"
            value: getValue()
        }
    }

    buttons: ButtonRow {
        style: ButtonStyle { }
        anchors.horizontalCenter: parent.horizontalCenter
        Button {
            text: "Close";
            onClicked: medalDialog.close();
        }
    }

    onStatusChanged: {
        if (status == DialogStatus.Closing) {
            medalDialog.destroy();
        }
    }
}
