import QtQuick 1.1
import com.nokia.meego 1.0
import "Storage.js" as Storage

QueryDialog  {
    id: soldierDeleteDialog

    property variant soldierIds: []

    icon: "img/confirmation_icon.png"
    titleText: "Confirmation"
    message: (soldierIds.length > 1) ? "Are you sure you wish to delete these soldiers?" : "Are you sure you wish to delete this soldier?"
    acceptButtonText: "Yes, delete"
    rejectButtonText: "Cancel"

    onAccepted: {
        for (var i = 0; i < soldierIds.length; i++) {
            Storage.deleteSoldier(soldierIds[i]);
        }
        pageStack.currentPage.cancelDelete();
        rootElement.refreshSoldiersData();
        if (!Storage.savedSoldiersExist()) { // if all soldiers are deleted, navigate to "soldier search" page
            pageStack.clear();
            pageStack.push(rootElement.getPage("soldier"), {showToolbar: false});
        }
    }

    onRejected: {
        pageStack.currentPage.cancelDelete();
    }

    onStatusChanged: {
        if (status == DialogStatus.Closing) {
            soldierDeleteDialog.destroy();
        }
    }
}
